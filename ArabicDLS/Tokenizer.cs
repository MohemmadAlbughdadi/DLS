﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using R = Arabic.Resources;
namespace ArabicDLS
{
    /// <summary>
    /// I need an Arabic Sentence Tokenizer
    /// </summary>
    public class Tokenizer
    {
        private string _Text = default(string);
        string[] spliters = { " ", ",", "-", "؟", Environment.NewLine };//a list of sentnce splitters in arabic sentence
        IEnumerable<object> resourceSet;
        public Tokenizer(string text)
        {
            _Text = text.Replace(Environment.NewLine, " ");
            resourceSet = R.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true).Cast<DictionaryEntry>().ToList().Select(x => x.Value);
        }
        /// <summary>
        /// To Split The Text Pass To Tokenizer By { , }  {/n} {space} and ignore anything that not related to arabic
        /// </summary>
        /// <returns>String Array of tokens</returns>

        private string[] Generate_Tokens()
        {

            var ret = _Text.Split('.').Select(x => new string(x.Where(y => resourceSet.Contains(y.ToString())).ToArray())).Where(x => !string.IsNullOrEmpty(x)).ToArray();
            return ret;
        }
        //The Start Button of this code from here the flow slides 
        public List<Syllable> Run()
        {
            var lines = Generate_Tokens();
            List<Syllable> all_syls = new List<Syllable>();
            foreach (var line in lines)
            {

                var firsttoken = true;
                string[] tokens = line.Split(spliters, StringSplitOptions.RemoveEmptyEntries).Select(x => new string(x.Where(y => resourceSet.Contains(y.ToString())).ToArray())).Where(x => !string.IsNullOrEmpty(x)).ToArray();
                foreach (var token in tokens)
                {
                    Syllabler s = new Syllabler(new Arabic.ArabicWord(token), firsttoken);
                    firsttoken = false;
                    var syles = s.Generate_Syllables();
                    foreach (var syl in syles)
                    {
                        all_syls.Add(syl);
                    }

                }
            }
            return all_syls;

        }

    }
}
