﻿using System.Collections.Generic;
using System.Linq;
using Arabic;
using Filters;
using System;

namespace ArabicDLS
{
    public class Syllabler
    {

        ArabicWord _token = null;
        public static bool _firsttoken;
        public Syllabler(ArabicWord token, bool firsttoken)
        {
            _token = token;
            _firsttoken = firsttoken;
        }
        /// <summary>
        /// Filter the Arabic Word through punch of Filters you can get rid of anyone
        /// </summary>
        /// <param name="w"></param>
        /// <returns></returns>
        private TokenFilter Filters(ArabicWord w)
        {
            
            TokenFilter res = new SpecialCaseFilter(w);
            res = new AlFilter(res);
            res = new ShadehFilter(res);
            res = new LaFilter(res);
            res = new WaFilter(res);
            res = new TanweenFilter(res);
            res = new CompatibleFilter(res);
            res = new HamzatFilter(res);
            res = new MaqsoraFilter(res);
            res = new VowelFilter(res);
            res = new OnStartFilter(res);
            res = new DiacCharFilter(res);
            res = new DefaultFilter(res);
            return res;
        }
        /// <summary>
        /// take a token and generate Syllables from it
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Syllable> Generate_Syllables()
        {
            var syles = new List<Syllable>();

            while (_token.Length != 0)
            {
                //here we can apply any filter we want but the Default_Filter at least Is Required
                var tokenfilter = Filters(_token);
                _token = tokenfilter?._tmptoken;
                TokenFilter.FirstFilter = true;
            }
            syles = TokenFilter.syllables;
            Syllable prev_syl = null;
            for (int i = 0; i < syles.Count(); i++)
            {
                var current_syl = syles.ElementAt(i);
                current_syl.Prev = prev_syl;
                current_syl.IsFirstToken = _firsttoken;
                if (i + 1 < syles.Count())
                {
                    current_syl.Next = syles.ElementAt(i + 1);
                }
                prev_syl = current_syl;

            }
            //لتغيير جهة اول حرف واخر حرف بالكلمات
            if (syles.First().GetSigns().Count() == 1 && syles.First().GetSigns().First()._Side != Side.Both && !syles.First().GetSigns().First().IsImportant)
            {
                syles.First().GetSigns().First()._Side = Side.Left;
            }
            if (syles.Last().GetSigns().Count() == 1 && syles.Last().GetSigns().First()._Side != Side.Both && syles.Last().Prev != null && !syles.Last().GetSigns().First().IsImportant)
            {
                var d = syles.Last().Prev.GetSigns().Last().AChar[syles.Last().Prev.GetSigns().Last().ImportantChar].Diac.AsEnum;
                var c1 = syles.Last().Prev.GetSigns().Last().AChar;
                var c2 = syles.Last().GetSigns().First().AChar;
                if (c1 != c2 && (d == Diacritics.sokon || d == Diacritics._none))
                    syles.Last().GetSigns().First()._Side = Side.Right;
            }
            syles.Add(new Syllable());
            TokenFilter.syllables = new List<Syllable>();
            TokenFilter.IsTheStart = true;

            return syles;
        }


    }
}
