﻿using System;
using Arabic;
using R = Arabic.Resources;
using SI = ArabicDLS.ImagesUrl;
using System.Linq;
namespace ArabicDLS
{
    /// <summary>
    /// The Simulator has left or right or both sides 
    /// </summary>
    public enum Side
    {
        nothing,
        Left,
        Right,
        Both
    }
    /// <summary>
    /// Each Syllable contain one or 2 Signs
    /// </summary>
    public class Sign
    {
        public bool IsGray { get; set; }//does the Sign need to be gray for some cases only
        public bool IsImportant { get; set; }//if the sign is Important to have certain side {i needed this when the client ask me to make last sign in left and first in right always but he changed his mind later}
        public bool IsFirstToken { get; set; } = false;// needed to check if this sign is in the first Token from the sentences
        public bool ConnectedToNext { get; set; } = false;//Connected When The next  char is Linkable
        public bool ConnectedToPrev { get; set; } = false;//Connected When The  prev char is Linkable
        public int ImportantChar { get; set; }//the index of the important character since the List of char in this sign may not be all importants{needed to get the Diact of this Sign}
        public ArabicChar[] AChar { get; set; } = new ArabicChar[10];//the sign may be in both hands Ex:لا
        public Side _Side { get; set; }
        public Sign(ArabicChar ac, Side s, bool Gray = false, bool Important = false)
        {
            AChar[0] = ac;
            _Side = s;
            IsGray = Gray;
            IsImportant = Important;
        }
        public Sign(ArabicChar[] ac, Side s, bool _Gray = false, bool _Important = false, int _importantchar = 0)
        {
            AChar = ac;
            _Side = s;
            ImportantChar = _importantchar;
            IsGray = _Gray;
            IsImportant = _Important;
        }


        public string GetSignDiac()
        {
            return AChar[ImportantChar].Diac.Diac;
        }

        public string GetSignDiacArrow()
        {
            switch (AChar[ImportantChar].Diac.AsEnum)
            {
                case Diacritics.fatha:
                    return SI.UpArrow;
                case Diacritics.kasra:
                    return SI.DownArrow;
                case Diacritics.ddameh:
                    return SI.FrontArrow;
                default:
                    return null;

            }
        }
        /// <summary>
        /// Preform the Sign to images
        /// </summary>
        /// <returns></returns>
        public Tuple<string, string> Perform_Sign()
        {
            var Image = CharImageSide(AChar, _Side, ImportantChar);
            return Image;
        }
        /// <summary>
        /// Map the Sign to images with take care of sides and the char and it's Diactricts and if it is gray or not
        /// </summary>
        /// <param name="ac"></param>
        /// <param name="s"></param>
        /// <param name="importantChar"></param>
        /// <returns></returns>
        private Tuple<string, string> CharImageSide(ArabicChar[] ac, Side s, int importantChar)
        {
            var fatha = ac[importantChar].Diac.AsEnum == Diacritics.fatha;
            var ddameh = ac[importantChar].Diac.AsEnum == Diacritics.ddameh;
            var kasra = ac[importantChar].Diac.AsEnum == Diacritics.kasra;
            Tuple<string, string> Image = null;
            var m = ac.ArCharArrayToString();
            var length = ac.Where(x => x != null && !string.IsNullOrEmpty(x.Char)).Count();
            if (length == 0 && ac[importantChar].Diac.IsTanween)
            {
                if (ac[importantChar].Diac.AsEnum == Diacritics.tnoen_ddameh)
                    Image = Tuple.Create(SI.Tnoeen_9ammeh, SI.Tnoeen_9ammeh);
                else if (ac[importantChar].Diac.AsEnum == Diacritics.tnoen_kasra)
                {
                    Image = Tuple.Create(SI.Tnoeen_Kasra, SI.Tnoeen_Kasra);
                }
                else if (ac[importantChar].Diac.AsEnum == Diacritics.tnoen_fatha)
                {
                    Image = Tuple.Create(SI.Tnoeen_Fat7a, SI.Tnoeen_Fat7a);
                }
            }
            #region Alshamseeh
            else if (length == 3 && (ac[0].Char + ac[1].Char).StartsWith(R.alef + R.lam))
            {
                if (IsGray)
                {
                    Image = Tuple.Create(SI.Al_Shamseh, SI.Al_Shamseh);
                }
                else
                {
                    Image = Tuple.Create(SI.Al_Shamseh_Without_Thumb, SI.Al_Shamseh_Without_Thumb);
                }
            }
            #endregion
            #region s is Both
            else if (length == 2 && (ac[0].Char + ac[1].Char).StartsWith(R.alef + R.lam))
            {
                Image = Tuple.Create(SI.Al_Qamareh, SI.Al_Qamareh);
            }
            else if (length == 2 && (ac[0].Char + ac[1].Char).StartsWith(R.wow + R.alef))
            {
                Image = Tuple.Create(SI.Waw_Aljamaa, SI.Waw_Aljamaa);
            }
            else if (ac[importantChar].Char.CompareTo(R.alef_maqsora) == 0)
            {
                Image = Tuple.Create(SI.Alef_Maqsora, SI.Alef_Maqsora);
            }
            #endregion
            #region La
            else if (length == 2 && (ac[0].Char + ac[1].Char).StartsWith(R.lam + R.alef))
            {

                Image = Tuple.Create(SI.La, SI.La);

            }
            #endregion
            #region Dealing With Haa
            else if (ac[importantChar].Char.CompareTo(R.haa) == 0)
            {

                if (fatha)
                {
                    Image = (ConnectedToNext && !ConnectedToPrev) ? Tuple.Create(SI.Haa_Mousola_Fat7a, SI.Haa_Mousola_Sokon) : Tuple.Create(SI.Haa_Fat7a, SI.Haa_Sokon);
                }
                else if (ddameh)
                {
                    Image = (ConnectedToNext && !ConnectedToPrev) ? Tuple.Create(SI.Haa_Mousola_9ammeh, SI.Haa_Mousola_Sokon) : Tuple.Create(SI.Haa_9ammeh, SI.Haa_Sokon);
                }
                else if (kasra)
                {
                    Image = (ConnectedToNext && !ConnectedToPrev) ? Tuple.Create(SI.Haa_Mousola_Kasra, SI.Haa_Mousola_Sokon) : Tuple.Create(SI.Haa_Kasra, SI.Haa_Sokon);

                }
                else
                {
                    Image = (ConnectedToNext && !ConnectedToPrev) ? Tuple.Create(SI.Haa_Mousola_Sokon, SI.Haa_Mousola_Sokon) : Tuple.Create(SI.Haa_Sokon, SI.Haa_Sokon);
                }

            }
            #endregion
            #region Dealing With letters and sides
            #region Tnoeen
            /* else if (ac.Diac.Diac.CompareTo(R.tnoen_9amma) == 0)
             {
                 Image = Tuple.Create(SI.Tnoeen_9ammeh, SI.Tnoeen_9ammeh);
             }
             else if (ac.Diac.Diac.CompareTo(R.tnoen_fatha) == 0)
             {
                 Image = Tuple.Create(SI.Tnoeen_Fat7a, SI.Tnoeen_Fat7a);
             }
             else if (ac.Diac.Diac.CompareTo(R.tnoen_kasra) == 0)
             {
                 Image = Tuple.Create(SI.Tnoeen_Kasra, SI.Tnoeen_Kasra);
             }*/
            #endregion
            else if (ac[importantChar].Char.CompareTo(R.hamzeh) == 0)
            {

                Image = Tuple.Create(SI.Hamze, SI.Hamze);


            }
            else if (ac[importantChar].Char.CompareTo(R.alef) == 0)
            {
                if (!IsFirstToken)
                {
                    if (fatha)
                        Image = Tuple.Create(SI.Alef_Hamze_Foq_Fat7a_Gray, SI.Alef_Hamze_Foq_Fat7a_Gray);
                    else if (ddameh)
                        Image = Tuple.Create(SI.Alef_Hamze_Foq_9ammeh_Gray, SI.Alef_Hamze_Foq_9ammeh_Gray);
                    else if (kasra)
                        Image = Tuple.Create(SI.Alef_Hamze_T7t_Gray, SI.Alef_Hamze_T7t_Gray);
                    else
                        Image = Tuple.Create(SI.Alef, SI.Alef);
                }
                else
                {
                    if (fatha)
                        Image = Tuple.Create(SI.Alef_Hamze_Foq_Fat7a, SI.Alef_Hamze_Foq_Fat7a);
                    else if (ddameh)
                        Image = Tuple.Create(SI.Alef_Hamze_Foq_9ammeh, SI.Alef_Hamze_Foq_9ammeh);
                    else if (kasra)
                        Image = Tuple.Create(SI.Alef_Hamze_T7t, SI.Alef_Hamze_T7t);
                    else
                        Image = Tuple.Create(SI.Alef, SI.Alef);
                }

            }
            else if (ac[importantChar].Char.CompareTo(R.alef_hamze_t7t) == 0)
            {
                Image = Tuple.Create(SI.Alef_Hamze_T7t, SI.Alef_Hamze_T7t);
            }
            else if (ac[importantChar].Char.CompareTo(R.alef_hamze_foq) == 0)
            {

                if (ddameh)
                {
                    Image = Tuple.Create(SI.Alef_Hamze_Foq_9ammeh, SI.Alef_Hamze_Foq_9ammeh);
                }
                else
                {
                    Image = Tuple.Create(SI.Alef_Hamze_Foq_Fat7a, SI.Alef_Hamze_Foq_Fat7a);
                }

            }
            else if (ac[importantChar].Char.CompareTo(R.alef_mamdodeh) == 0)
            {

                if (fatha)
                {
                    Image = Tuple.Create(SI.Alef_Hamze_Foq_Fat7a, SI.Alef_Hamze_Foq_Fat7a);
                }
                else if (ddameh)
                {
                    Image = Tuple.Create(SI.Alef_Hamze_Foq_9ammeh, SI.Alef_Hamze_Foq_9ammeh);
                }
                else if (kasra)
                {
                    Image = Tuple.Create(SI.Alef_Hamze_T7t, SI.Alef_Hamze_T7t); ;
                }
                else
                {
                    Image = Tuple.Create(SI.Alef, SI.Alef);
                }

            }
            else if (ac[importantChar].Char.CompareTo(R.baa) == 0)
            {

                {
                    if (fatha)
                    {
                        Image = Tuple.Create(SI.Baa_Fat7a, SI.Baa_Sokon);
                    }
                    else if (ddameh)
                    {
                        Image = Tuple.Create(SI.Baa_9ammeh, SI.Baa_Sokon);
                    }
                    else if (kasra)
                    {
                        Image = Tuple.Create(SI.Baa_Kasra, SI.Baa_Sokon);
                    }
                    else
                    {
                        Image = Tuple.Create(SI.Baa_Sokon, SI.Baa_Sokon);
                    }
                }

            }
            else if (ac[importantChar].Char.CompareTo(R.taa) == 0)
            {

                {
                    if (fatha)
                    {
                        Image = Tuple.Create(SI.Taa_Fat7a, SI.Taa_Sokon);
                    }
                    else if (ddameh)
                    {
                        Image = Tuple.Create(SI.Taa_9ammeh, SI.Taa_Sokon);
                    }
                    else if (kasra)
                    {
                        Image = Tuple.Create(SI.Taa_Kasra, SI.Taa_Sokon);
                    }
                    else
                    {
                        Image = Tuple.Create(SI.Taa_Sokon, SI.Taa_Sokon);
                    }
                }

            }
            else if (ac[importantChar].Char.CompareTo(R.taa_marbota) == 0)
            {

                {
                    if (fatha)
                    {
                        Image = Tuple.Create(SI.Taa_Marbota_Fat7a, SI.Taa_Marbota_Sokon);
                    }
                    else if (ddameh)
                    {
                        Image = Tuple.Create(SI.Taa_Marbota_9ammeh, SI.Taa_Marbota_Sokon);
                    }
                    else if (kasra)
                    {
                        Image = Tuple.Create(SI.Taa_Marbota_Kasra, SI.Taa_Marbota_Sokon);
                    }
                    else
                    {
                        Image = Tuple.Create(SI.Taa_Marbota_Sokon, SI.Taa_Marbota_Sokon);
                    }
                }

            }
            else if (ac[importantChar].Char.CompareTo(R.thaa) == 0)
            {

                {
                    if (fatha)
                    {
                        Image = Tuple.Create(SI.Thaa_Fat7a, SI.Thaa_Sokon);
                    }
                    else if (ddameh)
                    {
                        Image = Tuple.Create(SI.Thaa_9ammeh, SI.Thaa_Sokon);
                    }
                    else if (kasra)
                    {
                        Image = Tuple.Create(SI.Thaa_Kasra, SI.Thaa_Sokon);
                    }
                    else
                    {
                        Image = Tuple.Create(SI.Thaa_Sokon, SI.Thaa_Sokon);
                    }
                }

            }
            else if (ac[importantChar].Char.CompareTo(R.gem) == 0)
            {

                {
                    if (fatha)
                    {
                        Image = Tuple.Create(SI.Gem_Fat7a, SI.Gem_Sokon);
                    }
                    else if (ddameh)
                    {
                        Image = Tuple.Create(SI.Gem_9ammeh, SI.Gem_Sokon);
                    }
                    else if (kasra)
                    {
                        Image = Tuple.Create(SI.Gem_Kasra, SI.Gem_Sokon);
                    }
                    else
                    {
                        Image = Tuple.Create(SI.Gem_Sokon, SI.Gem_Sokon);
                    }
                }

            }
            else if (ac[importantChar].Char.CompareTo(R._7aa) == 0)
            {

                {
                    if (fatha)
                    {
                        Image = Tuple.Create(SI._7aa_Fat7a, SI._7aa_Sokon);
                    }
                    else if (ddameh)
                    {
                        Image = Tuple.Create(SI._7aa_9ammeh, SI._7aa_Sokon);
                    }
                    else if (kasra)
                    {
                        Image = Tuple.Create(SI._7aa_Kasra, SI._7aa_Sokon);
                    }
                    else
                    {
                        Image = Tuple.Create(SI._7aa_Sokon, SI._7aa_Sokon);
                    }
                }

            }
            else if (ac[importantChar].Char.CompareTo(R.khaa) == 0)
            {

                {
                    if (fatha)
                    {
                        Image = Tuple.Create(SI.Khaa_Fat7a, SI.Khaa_Sokon);
                    }
                    else if (ddameh)
                    {
                        Image = Tuple.Create(SI.Khaa_9ammeh, SI.Khaa_Sokon);
                    }
                    else if (kasra)
                    {
                        Image = Tuple.Create(SI.Khaa_Kasra, SI.Khaa_Sokon);
                    }
                    else
                    {
                        Image = Tuple.Create(SI.Khaa_Sokon, SI.Khaa_Sokon);
                    }
                }

            }
            else if (ac[importantChar].Char.CompareTo(R.dal) == 0)
            {

                {
                    if (fatha)
                    {
                        Image = Tuple.Create(SI.Dal_Fat7a, SI.Dal_Sokon);
                    }
                    else if (ddameh)
                    {
                        Image = Tuple.Create(SI.Dal_9ammeh, SI.Dal_Sokon);
                    }
                    else if (kasra)
                    {
                        Image = Tuple.Create(SI.Dal_Kasra, SI.Dal_Sokon);
                    }
                    else
                    {
                        Image = Tuple.Create(SI.Dal_Sokon, SI.Dal_Sokon);
                    }
                }

            }
            else if (ac[importantChar].Char.CompareTo(R.zal) == 0)
            {

                {
                    if (fatha)
                    {
                        Image = Tuple.Create(SI.Dhall_Fat7a, SI.Dhall_Sokon);
                    }
                    else if (ddameh)
                    {
                        Image = Tuple.Create(SI.Dhall_9ammeh, SI.Dhall_Sokon);
                    }
                    else if (kasra)
                    {
                        Image = Tuple.Create(SI.Dhall_Kasra, SI.Dhall_Sokon);
                    }
                    else
                    {
                        Image = Tuple.Create(SI.Dhall_Sokon, SI.Dhall_Sokon);
                    }
                }

            }
            else if (ac[importantChar].Char.CompareTo(R.raa) == 0)
            {

                {
                    if (fatha)
                    {
                        Image = Tuple.Create(SI.Raa_Fat7a, SI.Raa_Sokon);
                    }
                    else if (ddameh)
                    {
                        Image = Tuple.Create(SI.Raa_9ammeh, SI.Raa_Sokon);
                    }
                    else if (kasra)
                    {
                        Image = Tuple.Create(SI.Raa_Kasra, SI.Raa_Sokon);
                    }
                    else
                    {
                        Image = Tuple.Create(SI.Raa_Sokon, SI.Raa_Sokon);
                    }
                }

            }
            else if (ac[importantChar].Char.CompareTo(R.zay) == 0)
            {

                {
                    if (fatha)
                    {
                        Image = Tuple.Create(SI.Zay_Fat7a, SI.Zay_Sokon);
                    }
                    else if (ddameh)
                    {
                        Image = Tuple.Create(SI.Zay_9ammeh, SI.Zay_Sokon);
                    }
                    else if (kasra)
                    {
                        Image = Tuple.Create(SI.Zay_Kasra, SI.Zay_Sokon);
                    }
                    else
                    {
                        Image = Tuple.Create(SI.Zay_Sokon, SI.Zay_Sokon);
                    }
                }

            }
            else if (ac[importantChar].Char.CompareTo(R.sen) == 0)
            {

                {
                    if (fatha)
                    {
                        Image = Tuple.Create(SI.Sen_Fat7a, SI.Sen_Sokon);
                    }
                    else if (ddameh)
                    {
                        Image = Tuple.Create(SI.Sen_9ammeh, SI.Sen_Sokon);
                    }
                    else if (kasra)
                    {
                        Image = Tuple.Create(SI.Sen_Kasra, SI.Sen_Sokon);
                    }
                    else
                    {
                        Image = Tuple.Create(SI.Sen_Sokon, SI.Sen_Sokon);
                    }
                }

            }
            else if (ac[importantChar].Char.CompareTo(R.shen) == 0)
            {

                {
                    if (fatha)
                    {
                        Image = Tuple.Create(SI.Shen_Fat7a, SI.Shen_Sokon);
                    }
                    else if (ddameh)
                    {
                        Image = Tuple.Create(SI.Shen_9ammeh, SI.Shen_Sokon);
                    }
                    else if (kasra)
                    {
                        Image = Tuple.Create(SI.Shen_Kasra, SI.Shen_Sokon);
                    }
                    else
                    {
                        Image = Tuple.Create(SI.Shen_Sokon, SI.Shen_Sokon);
                    }
                }

            }
            else if (ac[importantChar].Char.CompareTo(R.sad) == 0)
            {

                {
                    if (fatha)
                    {
                        Image = Tuple.Create(SI.Sad_Fat7a, SI.Sad_Sokon);
                    }
                    else if (ddameh)
                    {
                        Image = Tuple.Create(SI.Sad_9ammeh, SI.Sad_Sokon);
                    }
                    else if (kasra)
                    {
                        Image = Tuple.Create(SI.Sad_Kasra, SI.Sad_Sokon);
                    }
                    else
                    {
                        Image = Tuple.Create(SI.Sad_Sokon, SI.Sad_Sokon);
                    }
                }

            }
            else if (ac[importantChar].Char.CompareTo(R.ddad) == 0)
            {

                {
                    if (fatha)
                    {
                        Image = Tuple.Create(SI.Daad_Fat7a, SI.Daad_Sokon);
                    }
                    else if (ddameh)
                    {
                        Image = Tuple.Create(SI.Daad_9ammeh, SI.Daad_Sokon);
                    }
                    else if (kasra)
                    {
                        Image = Tuple.Create(SI.Daad_Kasra, SI.Daad_Sokon);
                    }
                    else
                    {
                        Image = Tuple.Create(SI.Daad_Sokon, SI.Daad_Sokon);
                    }
                }

            }
            else if (ac[importantChar].Char.CompareTo(R._6aa) == 0)
            {

                {
                    if (fatha)
                    {
                        Image = Tuple.Create(SI._6aa_Fat7a, SI._6aa_Sokon);
                    }
                    else if (ddameh)
                    {
                        Image = Tuple.Create(SI._6aa_9ammeh, SI._6aa_Sokon);
                    }
                    else if (kasra)
                    {
                        Image = Tuple.Create(SI._6aa_Kasra, SI._6aa_Sokon);
                    }
                    else
                    {
                        Image = Tuple.Create(SI._6aa_Sokon, SI._6aa_Sokon);
                    }
                }

            }
            else if (ac[importantChar].Char.CompareTo(R.zhaa) == 0)
            {

                {
                    if (fatha)
                    {
                        Image = Tuple.Create(SI.Zhaa_Fat7a, SI.Zhaa_Sokon);
                    }
                    else if (ddameh)
                    {
                        Image = Tuple.Create(SI.Zhaa_9ammeh, SI.Zhaa_Sokon);
                    }
                    else if (kasra)
                    {
                        Image = Tuple.Create(SI.Zhaa_Kasra, SI.Zhaa_Sokon);
                    }
                    else
                    {
                        Image = Tuple.Create(SI.Zhaa_Sokon, SI.Zhaa_Sokon);
                    }
                }

            }
            else if (ac[importantChar].Char.CompareTo(R.aien) == 0)
            {

                {
                    if (fatha)
                    {
                        Image = Tuple.Create(SI.Aien_Fat7a, SI.Aien_Sokon);
                    }
                    else if (ddameh)
                    {
                        Image = Tuple.Create(SI.Aien_9ammeh, SI.Aien_Sokon);
                    }
                    else if (kasra)
                    {
                        Image = Tuple.Create(SI.Aien_Kasra, SI.Aien_Sokon);
                    }
                    else
                    {
                        Image = Tuple.Create(SI.Aien_Sokon, SI.Aien_Sokon);
                    }
                }

            }
            else if (ac[importantChar].Char.CompareTo(R.ghien) == 0)
            {

                if (fatha)
                {
                    Image = Tuple.Create(SI.Ghein_Fat7aa, SI.Ghein_Sokon);
                }
                else if (ddameh)
                {
                    Image = Tuple.Create(SI.Ghein_9ammeh, SI.Ghein_Sokon);
                }
                else if (kasra)
                {
                    Image = Tuple.Create(SI.Ghein_Kasra, SI.Ghein_Sokon);
                }
                else
                {
                    Image = Tuple.Create(SI.Ghein_Sokon, SI.Ghein_Sokon);
                }

            }
            else if (ac[importantChar].Char.CompareTo(R.faa) == 0)
            {

                {
                    if (fatha)
                    {
                        Image = Tuple.Create(SI.Faa_Fat7a, SI.Faa_Sokon);
                    }
                    else if (ddameh)
                    {
                        Image = Tuple.Create(SI.Faa_9ammeh, SI.Faa_Sokon);
                    }
                    else if (kasra)
                    {
                        Image = Tuple.Create(SI.Faa_Kasra, SI.Faa_Sokon);
                    }
                    else
                    {
                        Image = Tuple.Create(SI.Faa_Sokon, SI.Faa_Sokon);
                    }
                }

            }
            else if (ac[importantChar].Char.CompareTo(R.qaf) == 0)
            {
                if (fatha)
                {
                    Image = Tuple.Create(SI.Qaaf_Fat7a, SI.Qaaf_Sokon);
                }
                else if (ddameh)
                {
                    Image = Tuple.Create(SI.Qaaf_9ammeh, SI.Qaaf_Sokon);
                }
                else if (kasra)
                {
                    Image = Tuple.Create(SI.Qaaf_Kasra, SI.Qaaf_Sokon);
                }
                else
                {
                    Image = Tuple.Create(SI.Qaaf_Sokon, SI.Qaaf_Sokon);
                }
            }
            else if (ac[importantChar].Char.CompareTo(R.kaf) == 0)
            {

                {
                    if (fatha)
                    {
                        Image = Tuple.Create(SI.Kaf_Fat7a, SI.Kaf_Sokon);
                    }
                    else if (ddameh)
                    {
                        Image = Tuple.Create(SI.Kaf_9ammeh, SI.Kaf_Sokon);
                    }
                    else if (kasra)
                    {
                        Image = Tuple.Create(SI.Kaf_Kasra, SI.Kaf_Sokon);
                    }
                    else
                    {
                        Image = Tuple.Create(SI.Kaf_Sokon, SI.Kaf_Sokon);
                    }
                }

            }

            else if (ac[importantChar].Char.CompareTo(R.lam) == 0)
            {
                if (fatha)
                {
                    Image = Tuple.Create(SI.Lam_Fat7a, SI.Lam_Sokon);
                }
                else if (ddameh)
                {
                    Image = Tuple.Create(SI.Lam_9ammeh, SI.Lam_Sokon);
                }
                else if (kasra)
                {
                    Image = Tuple.Create(SI.Lam_Kasra, SI.Lam_Sokon);
                }
                else
                {
                    Image = Tuple.Create(SI.Lam_Sokon, SI.Lam_Sokon);
                }
            }
            else if (ac[importantChar].Char.CompareTo(R.mem) == 0)
            {
                if (fatha)
                {
                    Image = Tuple.Create(SI.Mem_Fat7a, SI.Mem_Sokon);
                }
                else if (ddameh)
                {
                    Image = Tuple.Create(SI.Mem_9ammeh, SI.Mem_Sokon);
                }
                else if (kasra)
                {
                    Image = Tuple.Create(SI.Mem_Kasra, SI.Mem_Sokon);
                }
                else
                {
                    Image = Tuple.Create(SI.Mem_Sokon, SI.Mem_Sokon);
                }
            }
            else if (ac[importantChar].Char.CompareTo(R.non) == 0)
            {
                if (fatha)
                {
                    Image = Tuple.Create(SI.Non_Fat7a, SI.Non_Sokon);
                }
                else if (ddameh)
                {
                    Image = Tuple.Create(SI.Non_9ammeh, SI.Non_Sokon);
                }
                else if (kasra)
                {
                    Image = Tuple.Create(SI.Non_Kasra, SI.Non_Sokon);
                }
                else
                {
                    Image = Tuple.Create(SI.Non_Sokon, SI.Non_Sokon);
                }
            }

            else if (ac[importantChar].Char.CompareTo(R.wow) == 0)
            {
                if (fatha)
                {
                    Image = Tuple.Create(SI.Waw_Fat7a, SI.Waw_Sokon);
                }
                else if (ddameh)
                {
                    Image = Tuple.Create(SI.Waw_9ammeh, SI.Waw_Sokon);
                }
                else if (kasra)
                {
                    Image = Tuple.Create(SI.Waw_Kasra, SI.Waw_Sokon);
                }
                else
                {
                    Image = Tuple.Create(SI.Waw_Sokon, SI.Waw_Sokon);
                }
            }
            else if (ac[importantChar].Char.CompareTo(R.wow_hamzeh) == 0)
            {
                Image = Tuple.Create(SI.Waw_Hamze_Foq, SI.Waw_Hamze_Foq);
            }

            else if (ac[importantChar].Char.CompareTo(R.yaa) == 0)
            {
                if (fatha)
                {
                    Image = Tuple.Create(SI.Yaa_Fat7a, SI.Yaa_Sokon);
                }
                else if (ddameh)
                {
                    Image = Tuple.Create(SI.Yaa_9ammeh, SI.Yaa_Sokon);
                }
                else if (kasra)
                {
                    Image = Tuple.Create(SI.Yaa_Kasra, SI.Yaa_Sokon);
                }
                else
                {
                    Image = Tuple.Create(SI.Yaa_Sokon, SI.Yaa_Sokon);
                }
            }
            else if (ac[importantChar].Char.CompareTo(R.yaa_hamzeh) == 0)
            {
                Image = Tuple.Create(SI.Yaa_Hamze_Foq, SI.Yaa_Hamze_Foq);
            }
            #endregion
            else
            {
                Image = Tuple.Create(SI.Jism_Salah, SI.Jism_Salah);
            }
            return Image;

        }

    }

}
