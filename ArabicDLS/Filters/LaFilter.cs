﻿using ArabicDLS;
using Arabic;
using R = Arabic.Resources;

namespace Filters
{
    /// <summary>
    /// بما ان لا تعتبر حالة خاصة لهيك اضطريت اعملها فلتر خاص
    /// </summary>
    class LaFilter : TokenFilter
    {
        public LaFilter(TokenFilter tf) : this(tf._tmptoken)
        {

        }
        public LaFilter(ArabicWord token) : base(token)
        {

            if (FirstFilter)
            {
                var tmp = token;
                //في حال كانت -لا- بأول الكلمة المقطعة او كان في محرف وبعده -لا - ادخل بالشرط
                if ((tmp.Length > 1 && (tmp[0].Char + tmp[1].Char).Equals(R.lam + R.alef)) || (tmp.Length > 2 && (tmp[1].Char + tmp[2].Char).Equals(R.lam + R.alef)))
                {
                    //اذا كان في محرف وبعده -لا- ادخل بالشرط
                    if (tmp.Length > 2 && (tmp[1].Char + tmp[2].Char).Equals(R.lam + R.alef))
                    {
                        syllables.Add(new Syllable(new Sign(tmp[0], Side.Left)),GetType().Name);
                        _tmptoken = token = tmp = tmp.Remove(1);
                    }
                    var t0 = new ArabicChar(tmp[0].Char, new Diacritic(), tmp[0].HasShadeh, tmp[0].ParentWord);
                    var t1 = new ArabicChar(tmp[1].Char, new Diacritic(), tmp[1].HasShadeh, tmp[1].ParentWord);
                    syllables.Add(new Syllable(new Sign(new ArabicChar[] { t0, t1 }, Side.Right)),GetType().Name);
                    _tmptoken = tmp.Remove(2);
                    //حدث تغير على النص الأصلي معناته انا مالي بأول الكلمة

                    if (IsTheStart)
                        IsTheStart = false;
                    //لضمان عمل فلتر واحد كل دورة

                    FirstFilter = false;
                }

            }
        }
    }
}
