﻿using ArabicDLS;
using Arabic;
using R = Arabic.Resources;


namespace Filters
{
    /// <summary>
    /// بما ان ى حالة خاصة اضطريت اعملها فلتر خاص
    /// </summary>
    class MaqsoraFilter : TokenFilter
    {
        public MaqsoraFilter(TokenFilter tf) : this(tf._tmptoken)
        {

        }
        public MaqsoraFilter(ArabicWord token) : base(token)
        {
            if (FirstFilter)
            {
                var tmp = token;
                if (tmp[0].Char.Equals(R.alef_maqsora) || tmp.Length > 1 && tmp[1].Char.Equals(R.alef_maqsora))
                {
                    if (tmp.Length > 1 && tmp[1].Char.Equals(R.alef_maqsora))
                    {
                        syllables.Add(new Syllable(new Sign(tmp[0], Side.Left)),GetType().Name);
                        _tmptoken = token = token.Remove(1);
                    }
                    syllables.Add(new Syllable(new Sign(token[0], Side.Both)),GetType().Name);
                    _tmptoken = token.Remove(1);
                    if (IsTheStart)
                        IsTheStart = false;
                    FirstFilter = false;
                }
            }
        }
    }
}
