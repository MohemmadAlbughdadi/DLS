﻿using System.Collections.Generic;
using ArabicDLS;
using System.Linq;
using Arabic;
using R = Arabic.Resources;


namespace Filters
{
    /// <summary>
    /// اب لكل الفلاتر اي فلتر يجب ان يرث منه ويحقق شروط معينة تجدها موضوعة دائما في الفلاتر
    /// يحتوي ايضا على مجموعة ميثودات مساعدة
    /// يحتوي على المتحولات العامة على مستوى العملية كاملة
    /// </summary>
    public static class ExtendSyllableList
    {
        public static void Add(this List<Syllable> list, Syllable s, string classname)
        {
            list.Add(s);
        }
    }
    public static class ExtendArabicWord
    {
        public static bool IsCompatible(this ArabicWord ac, bool isTheStart)
        {
            var b = ac.Length > 1 && (new string[] { R.yaa, R.alef, R.wow }.Any(x => x.Equals(ac[1].Char) && !x.Equals(ac[0]))) && !ac[0].HasDiac && !ac[1].HasDiac;
            return (ac.Length > 1 && ((ac[1].HasSokonOrNone) && ((ac[0].IsHamzeh && isTheStart && !NotIsCompatible(ac.Remove(1)) && !IsCompatible(ac.Remove(1),false)) || ac[0].Diac.AsEnum == Diacritics.fatha && ac[1].Char == R.alef || ac[0].Diac.AsEnum == Diacritics.ddameh && ac[1].Char == R.wow || ac[0].Diac.AsEnum == Diacritics.kasra && ac[1].Char == R.yaa))) ||
                   (b);
        }
        public static bool NotIsCompatible(this ArabicWord ac)
        {
            return (ac.Length > 1 && ac[0].Diac.AsEnum != Diacritics.fatha && ac[1].Char == R.alef || ac[0].Diac.AsEnum != Diacritics.ddameh && ac[1].Char == R.wow || ac[0].Diac.AsEnum != Diacritics.kasra && ac[1].Char == R.yaa) && !ac[0].HasSokonOrNone;
        }
    }
    public abstract partial class TokenFilter
    {
        #region MainFilterMethods
        protected internal TokenFilter(ArabicWord token)
        {
            _tmptoken = token;
            if (IsTheStart)
            {
                _token = token;
            }
        }
        protected internal TokenFilter(TokenFilter F) : this(F._tmptoken)
        {

        }


        #endregion
    }

    abstract partial class TokenFilter
    {
        /// <summary>
        /// to check any filteration has been done before
        /// </summary>
        public static bool IsTheStart = true;
        public static bool FirstFilter = true;
        public ArabicWord _tmptoken;
        public static ArabicWord _token;
        public static List<Syllable> syllables = new List<Syllable>();



    }
}
