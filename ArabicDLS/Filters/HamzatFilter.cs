﻿using Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arabic;
using ArabicDLS;
namespace Filters
{
    /// <summary>
    /// فلتر للهمزات 
    /// </summary>
    class HamzatFilter : TokenFilter
    {
        public HamzatFilter(TokenFilter token) : this(token._tmptoken)
        {
        }
        public HamzatFilter(ArabicWord token) : base(token)
        {
            if (FirstFilter)
            {
                var tmp = token;
                if ((tmp[0].IsHamzeh && tmp.Length > 1) || (tmp.Length > 2 && tmp[1].IsHamzeh))
                {
                    if (tmp.Length > 2 && tmp[1].IsHamzeh)
                    {
                        syllables.Add(new Syllable(new Sign(tmp[0], Side.Left)), GetType().Name);
                        _tmptoken = token = tmp = tmp.Remove(1);
                        //حدث تغير على النص الأصلي معناته انا مالي بأول الكلمة
                        if (IsTheStart)
                            IsTheStart = false;
                        //لضمان عمل فلتر واحد كل دورة

                        FirstFilter = false;
                    }
                }
            }
        }


    }
}
