﻿using ArabicDLS;
using Arabic;


namespace Filters
{
    /// <summary>
    /// في حال الفلاتر ما اشتغلت هاد رح يشتغل اخر الشي
    /// </summary>
    class DefaultFilter : TokenFilter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Second"></param>
        /// <param name="First"></param>
        /// <returns>1 if the first was haa and the second not linkable with it
        /// -1 if the second was haa and the first not linkable with it
        /// 0 Otherwise</returns>

        public DefaultFilter(TokenFilter tf) : this(tf._tmptoken)
        {

        }
        public DefaultFilter(ArabicWord token) : base(token)
        {
            if (FirstFilter)
            {
                var tmp = token;
                //اذا كان حرف واحد
                if (tmp.Length == 1)
                {
                    //بَّ
                    //بّ
                    //بَ

                    //ب
                    if (tmp[0].IsArabic || tmp[0].HasDiac || tmp[0].HasShadeh)
                    {
                        syllables.Add(new Syllable(new Sign(tmp[0], Side.Left)), GetType().Name);

                        _tmptoken = token.Remove(1);
                    }
                    if (IsTheStart)
                        IsTheStart = false;
                    FirstFilter = false;
                }

                else if (tmp.Length == 2)
                {
                    syllables.Add(new Syllable(new Sign(tmp[0], Side.Left)), GetType().Name);
                    syllables.Add(new Syllable(new Sign(tmp[1], Side.Right)), GetType().Name);
                    _tmptoken = token.Remove(2);
                    if (IsTheStart)
                        IsTheStart = false;
                    FirstFilter = false;
                }
                else if (tmp.Length > 2)
                {
                    //نأخذ الحرفين الثاني والثالث

                    //في حال الثالث 
                    if (tmp[0].IsVowel || !tmp[1].IsVowel)
                    {
                        syllables.Add(new Syllable(new Sign(tmp[0], Side.Left)), GetType().Name);
                        syllables.Add(new Syllable(new Sign(tmp[1], Side.Right)), GetType().Name);
                        _tmptoken = tmp.Remove(2);
                    }
                    else if (!tmp[0].IsVowel || tmp[1].IsVowel)
                    {
                        syllables.Add(new Syllable(new Sign(tmp[0], Side.Right)), GetType().Name);
                        _tmptoken = tmp.Remove(1);
                    }
                    else
                    {
                        syllables.Add(new Syllable(new Sign(tmp[0], Side.Left)), GetType().Name);
                        _tmptoken = tmp.Remove(1);
                    }
                    if (IsTheStart)
                        IsTheStart = false;
                    FirstFilter = false;


                }
            }
        }

    }
}
