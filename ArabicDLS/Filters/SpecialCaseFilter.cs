﻿using Arabic;
using Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using R = Arabic.Resources;

namespace ArabicDLS
{
    /// <summary>
    /// بعض الكلمات لها حالات خاصة 
    /// مثال كلمة الله الكمبيوتر لا يعتبر ان هناك شدة
    /// </summary>
    class SpecialCaseFilter : TokenFilter
    {
        public SpecialCaseFilter(TokenFilter tf) : this(tf._tmptoken)
        {

        }
        public SpecialCaseFilter(ArabicWord token) : base(token)
        {
            if (FirstFilter)
            {
                var tmp = token;
                var enterd = false;
                if (tmp.Filterd.GetArabicString.Equals(R.ALLAH) || tmp.Remove(1).Filterd.GetArabicString.Equals(R.ALLAH))
                {
                    //to fix a problem with the spcial case word الله where there is no shadeh in it when parsing
                    if (tmp.Remove(1).Filterd.GetArabicString.Equals(R.ALLAH))//بالله
                    {
                        tmp[3].HasShadeh = true;
                    }
                    else
                        tmp[2].HasShadeh = true;//الله
                    _tmptoken = tmp;
                }

            }
        }
    }
}
