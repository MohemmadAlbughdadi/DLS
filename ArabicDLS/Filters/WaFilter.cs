﻿using ArabicDLS;
using Arabic;
using R = Arabic.Resources;


namespace Filters
{
    /// <summary>
    /// لأن الحاكي يعتبر وا عدة حالات منها في وسط الكلمة منها وا الجماعة اضطريت ان اخصص فلتر
    /// </summary>
    class WaFilter : TokenFilter
    {
        public WaFilter(TokenFilter tf) : this(tf._tmptoken)
        {

        }
        public WaFilter(ArabicWord token) : base(token)
        {

            if (FirstFilter)
            {
                var tmp = token;
                var filterd = tmp.Filterd;
                if ((tmp.Length > 1 && (filterd[0] + filterd[1]).Equals(R.wow + R.alef)) || (tmp.Length > 2 && (filterd[1] + filterd[2]).Equals(R.wow + R.alef)))
                {
                    if (tmp.Length > 2 && (filterd[1] + filterd[2]).Equals(R.wow + R.alef))
                    {
                        syllables.Add(new Syllable(new Sign(tmp[0], Side.Left)),GetType().Name);
                        _tmptoken = token = tmp = tmp.Remove(1);
                    }

                    if (!IsTheStart && tmp.Length == 2)
                    {
                        var t0 = new ArabicChar(tmp[0].Char, new Diacritic(), tmp[0].HasShadeh, tmp[0].ParentWord);
                        var t1 = new ArabicChar(tmp[1].Char, new Diacritic(), tmp[1].HasShadeh, tmp[1].ParentWord);
                        syllables.Add(new Syllable(new Sign(new ArabicChar[] { t0, t1 }, Side.Both)),GetType().Name);
                        _tmptoken = token.Remove(2);
                    }
                    else
                    {
                        syllables.Add(new Syllable(new Sign(tmp[0], Side.Left), new Sign(tmp[1], Side.Right)),GetType().Name);
                        _tmptoken = token.Remove(2);
                    }
                    if (IsTheStart)
                        IsTheStart = false;
                    FirstFilter = false;
                }
            }

        }

    }
}
