﻿using Arabic;
using ArabicDLS;
using R = Arabic.Resources;

namespace Filters
{
    /// <summary>
    /// لتصفية ال الشمسية والقمرية والتي في وسط الجملة
    /// </summary>
    class AlFilter : TokenFilter
    {
        public AlFilter(TokenFilter tf) : this(tf._tmptoken)
        {

        }
        public AlFilter(ArabicWord token) : base(token)
        {
            if (FirstFilter)
            {
                //خال
                //القمر
                //بخال
                //بالقمر
                //الله
                var tmp = token;
                // ا ل لّ ه

                //فحص فيما كانت الكلمة تحتوي على ال او ال قادمة بعد حرف
                if ((tmp.Length > 1 && (tmp[0].Char + tmp[1].Char).Equals(R.alef + R.lam)) || (tmp.Length > 2 && (tmp[1] + tmp[2]).Equals(R.alef + R.lam)) && IsTheStart)
                {
                    if (tmp.Length > 2 && (tmp[1] + tmp[2]).Equals(R.alef + R.lam))
                    {
                        syllables.Add(new Syllable(new Sign(tmp[0], Side.Left)),GetType().Name);
                        _tmptoken = token = tmp = tmp.Remove(1);

                        //حدث تغير على النص الأصلي معناته انا مالي بأول الكلمة
                        if (IsTheStart)
                            IsTheStart = false;
                        //لضمان عمل فلتر واحد كل دورة
                        FirstFilter = false;
                    }
                    //شوف تاني حرف
                    //في حال كانت اللام عليها شدة او لأ
                    ArabicChar x = null;
                    if (tmp[1].HasShadeh)
                        x = tmp[1];
                    else if (tmp.Length > 2)
                        x = tmp[2];
                    if (x != null)
                    {
                        //اذا كان ينتمي لمجموعة الأحرف الشمسية
                        if (x.IsShamseeh)
                        {
                            var t0 = new ArabicChar(tmp[0].Char, new Diacritic(), tmp[0].HasShadeh, tmp[0].ParentWord);
                            var t1 = new ArabicChar(tmp[1].Char, new Diacritic(), tmp[1].HasShadeh, tmp[1].ParentWord);
                            syllables.Add(new Syllable(new Sign(new ArabicChar[] { t0, t1, x }, Side.Both, Syllabler._firsttoken, false, 2)),GetType().Name);
                            //_tmptoken = token.Remove(tmp[1].HasShadeh ? 2 : 3);
                            _tmptoken = token.Remove(3);

                            //ضيف الحرف الشمسي للبداية
                            /*var t = new ArabicChar(x.Char, x.Diac, false, x.ParentWord);
                            _tmptoken = _tmptoken.InsertAt(t, 0);*/
                        }

                        //اذا كان مو شمسي معناته قمري
                        else
                        {
                            //اذا كانت ببداية الجملة وببداية الكلام
                            if (IsTheStart && Syllabler._firsttoken)
                                syllables.Add(new Syllable(new Sign(tmp[0], Side.Left), new Sign(tmp[1], Side.Right)),GetType().Name);
                            //اذا لم تكن ببداية الجملة او كانت بسياق الكلام
                            else
                            {
                                var t0 = new ArabicChar(tmp[0].Char, new Diacritic(), tmp[0].HasShadeh, tmp[0].ParentWord);
                                var t1 = new ArabicChar(tmp[1].Char, new Diacritic(), tmp[1].HasShadeh, tmp[1].ParentWord);
                                syllables.Add(new Syllable(new Sign(new ArabicChar[] { t0, t1 }, Side.Both)),GetType().Name);
                            }

                            _tmptoken = token.Remove(2);
                        }
                        //حدث تغير على النص الأصلي معناته انا مالي بأول الكلمة
                        if (IsTheStart)
                            IsTheStart = false;
                        //لضمان عمل فلتر واحد كل دورة
                        FirstFilter = false;
                    }
                }


            }
        }
    }

}
