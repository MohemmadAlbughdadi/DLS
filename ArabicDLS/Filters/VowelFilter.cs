﻿using ArabicDLS;
using Arabic;

namespace Filters
{
    /// <summary>
    /// فلتر خاص بالأحرف الصوتية مثل ا و ي
    /// لأنهم لديهم حالات خاصة احيانا
    /// </summary>
    class VowelFilter : TokenFilter
    {

        public VowelFilter(TokenFilter tf) : this(tf._tmptoken)
        {

        }
        public VowelFilter(ArabicWord token) : base(token)
        {
            if (FirstFilter)
            {
                var tmp = token;
                var withsokon = tmp.GetArabicWordWithSokonByDefault;
                if (tmp.Length > 1 && tmp[1].IsVowel)
                {
                    if (tmp[0].HasDiacWithoutSokon)
                    {
                        syllables.Add(new Syllable(new Sign(withsokon[0], Side.Left)), GetType().Name);
                        syllables.Add(new Syllable(new Sign(withsokon[1], Side.Right)), GetType().Name);
                    }
                    else
                    {
                        syllables.Add(new Syllable(new Sign(withsokon[0], Side.Left), new Sign(withsokon[1], Side.Right)), GetType().Name);
                    }
                    _tmptoken = withsokon.Remove(2);

                    if (IsTheStart)
                        IsTheStart = false;
                    FirstFilter = false;
                }

            }
        }
    }
}
