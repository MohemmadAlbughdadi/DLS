﻿using Arabic;
using ArabicDLS;

namespace Filters
{
    /// <summary>
    /// فلتر خاص بالأحرف التي تحتوي تنوين
    /// </summary>
    class TanweenFilter : TokenFilter
    {
        public TanweenFilter(TokenFilter tf) : this(tf._tmptoken)
        {

        }
        public TanweenFilter(ArabicWord token) : base(token)
        {
            if (FirstFilter)
            {
                var tmp = token;
                if (tmp[0].HasTanween || tmp.Length > 1 && tmp[1].HasTanween)
                {
                    if (tmp.Length > 1 && tmp[1].HasTanween)
                    {
                        syllables.Add(new Syllable(new Sign(tmp[0], Side.Left)),GetType().Name);
                        _tmptoken = token = tmp = tmp.Remove(1);

                    }
                    var t = new ArabicChar(tmp[0].Char, new Diacritic(), false, tmp[0].ParentWord);
                    var tt = new ArabicChar("", tmp[0].Diac, false, tmp[0].ParentWord);
                    syllables.Add(new Syllable(new Sign(t, Side.Left), new Sign(tt, Side.Right)),GetType().Name);
                    _tmptoken = token = tmp.Remove(1);
                    //حدث تغير على النص الأصلي معناته انا مالي بأول الكلمة

                    if (IsTheStart)
                        IsTheStart = false;
                    //لضمان عمل فلتر واحد كل دورة

                    FirstFilter = false;
                }
            }
        }

    }
}
