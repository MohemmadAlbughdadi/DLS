﻿using ArabicDLS;
using Arabic;


namespace Filters
{
    class OnStartFilter : TokenFilter
    {
        /// <summary>
        /// للأحرف التي تأتي ببداية كلمة حالات خاصة رح نتعامل معها هنا
        /// </summary>
        /// <param name="tf"></param>
        public OnStartFilter(TokenFilter tf) : this(tf._tmptoken)
        {

        }
        public OnStartFilter(ArabicWord token) : base(token)
        {
            if (FirstFilter)
            {
                var tmp = token;
                //في حال كان عندي كلمة طولهااكبر من محرف واحد
                if (IsTheStart && tmp.Length > 1)
                {
                    var withsokon = tmp.GetArabicWordWithSokonByDefault;

                    //اذا كان اول محرف هو اما -أ و ي- او في حال كان همزة وصل او همزة قطع
                    if (withsokon[0].IsVowel)
                    {
                        if (withsokon.Length > 2 && !withsokon[2].IsVowel)
                        {
                            if (withsokon[2].HasSokon)
                                syllables.Add(new Syllable(new Sign(withsokon[0], Side.Left), new Sign(withsokon[1], Side.Right)),GetType().Name);
                            else if (withsokon[1].HasSokon && withsokon[2].HasDiacWithoutSokon)
                            {
                                syllables.Add(new Syllable(new Sign(withsokon[0], Side.Left), new Sign(withsokon[1], Side.Right)),GetType().Name);
                            }
                            else
                            {
                                syllables.Add(new Syllable(new Sign(withsokon[0], Side.Left)),GetType().Name);

                                syllables.Add(new Syllable(new Sign(withsokon[1], Side.Right)),GetType().Name);
                            }
                            _tmptoken = tmp.Remove(2);
                        }
                        else if (withsokon[1].HasDiacWithoutSokon)
                        {
                            syllables.Add(new Syllable(new Sign(withsokon[0], Side.Left)),GetType().Name);
                            syllables.Add(new Syllable(new Sign(withsokon[1], Side.Right)),GetType().Name);
                            _tmptoken = tmp.Remove(2);
                        }
                        else
                        {
                            syllables.Add(new Syllable(new Sign(withsokon[0], Side.Left)),GetType().Name);
                            _tmptoken = tmp.Remove(1);
                        }
                        IsTheStart = false;
                        FirstFilter = false;
                    }
                    //في حال كان الحرف اول ليس حرف مد والحرف الذي بعده هو حرف مد متحرك بغير حركته
                    /* else if (!Is_Vowle(filterd[0]) && ((Char.Parse(R.wow) == nextchar.Item1 && Has_Diac(withsokon, nextchar.Item2).Item2 != Char.Parse(R._9ameh)) || (Char.Parse(R.yaa) == nextchar.Item1 && Has_Diac(withsokon, nextchar.Item2).Item2 != Char.Parse(R.kasra))))
                     {
                         syllables.Add(new Syllable(new Sign(filterd[0], GetCharDiac(withsokon, 0).Item1, Side.right)),GetType().Name);
                         syllables.Add(new Syllable(new Sign(nextchar.Item1, Has_Diac(withsokon, nextchar.Item2).Item2, Side.left)),GetType().Name);

                         var next2Chars = GetNextNChar_WithDiac(Remove_Substring_withDiac(withsokon, 2), 2);

                         if (Is_Compatible(next2Chars) == 0 && nextnextchar != null)
                         {
                             syllables.Add(new Syllable(new Sign(GetNextArChar(next2Chars, 0).Item1, Has_Diac(next2Chars, GetNextArChar(next2Chars, GetNextArChar(next2Chars, 0).Item2 + 1).Item2).Item2, Side.left)),GetType().Name);
                             _token = Remove_Substring_withDiac(token, 3);

                         }
                         else if (Is_Compatible(next2Chars) == 1)
                         {
                             var x1 = GetNextArChar(next2Chars, 0);
                             var x2 = GetNextArChar(next2Chars, x1.Item2 + 1);
                             syllables.Add(new Syllable(new Sign(x1.Item1, Has_Diac(next2Chars, x1.Item2).Item2, Side.left), new Sign(x2.Item1, Has_Diac(next2Chars, x2.Item2).Item2, Side.right)),GetType().Name);
                             _token = Remove_Substring_withDiac(token, 4);

                         }

                         Is_TheStart = false;
                         FirstFilter = false;
                     }
                    //اذا كان اول حرف يبدأ بحركة
                    else if (Has_Diac(token, 0).Item1 == 1)
                    {
                        var t1 = GetNextArChar(withsokon, 1);
                        var t2 = GetNextArChar(withsokon, t1.Item2 + 1);
                        var x1 = Has_Diac(withsokon, t1.Item2).Item2;
                        var x2 = Has_Diac(token, 0).Item2;
                        if ((Has_Sokon(withsokon, t1.Item2) == 1 || x1.CompareTo(x2) == 0) && t2 != null && !(Has_Sokon(withsokon, t2.Item2) == 1))
                        {
                            if (filterd.Length > 2 && !Is_Vowle(filterd[2]))
                            {
                                if (GetCharDiac(token, 0).Item1 == GetCharDiac(token, GetNextArChar(token, 1).Item2).Item1 == (char.Parse(R.fatha) == GetCharDiac(token, GetNextArChar(token, 1).Item2).Item1))
                                {
                                    syllables.Add(new Syllable(new Sign(filterd[0], GetCharDiac(token, 0).Item1, Side.right), new Sign(filterd[1], GetCharDiac(token, GetNextArChar(token, 1).Item2).Item1, Side.left)),GetType().Name);
                                }
                                syllables.Add(new Syllable(new Sign(filterd[2], GetCharDiac(token, GetCharDiac(token, GetNextArChar(token, 1).Item2).Item2 + 1).Item1, Side.left)),GetType().Name);

                                _token = Remove_Substring_withDiac(token, 3);
                            }

                            else
                            {
                                syllables.Add(new Syllable(new Sign(filterd[0], GetCharDiac(token, 0).Item1, Side.right)),GetType().Name);

                                _token = Remove_Substring_withDiac(token, 1);
                            }
                            //حدث تغير على النص الأصلي معناته انا مالي بأول الكلمة

                            Is_TheStart = false;
                            //لضمان عمل فلتر واحد كل دورة
                            FirstFilter = false;
                        }

                    }*/
                }
            }
        }

    }
}
