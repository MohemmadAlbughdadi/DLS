﻿using Arabic;
using ArabicDLS;
using Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filters
{
    /// <summary>
    /// لفحص اذا كان الحرفان متوافقان ام متعارضان مع بعض 
    /// في حالة الأحرف والهمزات والحركات
    /// </summary>
    public class CompatibleFilter : TokenFilter
    {
        public CompatibleFilter(TokenFilter token) : this(token._tmptoken)
        {

        }
        public CompatibleFilter(ArabicWord token) : base(token)
        {
            if (FirstFilter)
            {
                var tmp = token;
                var withsokon = tmp;
                var entered = false;
                if (withsokon.Length >= 2 && withsokon.IsCompatible(IsTheStart))
                {
                    syllables.Add(new Syllable(new Sign(withsokon[0], Side.Left), new Sign(withsokon[1], Side.Right)), GetType().Name);
                    _tmptoken = withsokon.Remove(2);
                    entered = true;
                }


                else if (withsokon.Length > 2 && withsokon.Remove(1).IsCompatible(IsTheStart))
                {
                    if (withsokon[0].HasSokonOrNone)
                        syllables.Add(new Syllable(new Sign(withsokon[0], Side.Right)), GetType().Name);
                    else
                        syllables.Add(new Syllable(new Sign(withsokon[0], Side.Left)), GetType().Name);//الرَّحِيمِ to make the رَ with left

                    syllables.Add(new Syllable(new Sign(withsokon[1], Side.Left), new Sign(withsokon[2], Side.Right)), GetType().Name);
                    _tmptoken = withsokon.Remove(3);
                    entered = true;
                }
                else if (withsokon.Length >= 2 && withsokon.NotIsCompatible())
                {
                    syllables.Add(new Syllable(new Sign(withsokon[0], Side.Left, false, true)), GetType().Name);
                    _tmptoken = withsokon.Remove(1);
                    entered = true;
                }
                else if (withsokon.Length > 2 && withsokon.Remove(1).NotIsCompatible())
                {
                    syllables.Add(new Syllable(new Sign(withsokon[0], Side.Left)), GetType().Name);
                    syllables.Add(new Syllable(new Sign(withsokon[1], Side.Right, false, true)), GetType().Name);
                    _tmptoken = withsokon.Remove(2);
                    entered = true;
                }

                if (entered)
                {
                    //حدث تغير على النص الأصلي معناته انا مالي بأول الكلمة
                    if (IsTheStart)
                        IsTheStart = false;
                    //لضمان عمل فلتر واحد كل دورة

                    FirstFilter = false;
                }
            }
        }
    }
}
