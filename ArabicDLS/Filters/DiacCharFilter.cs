﻿using Arabic;

using R = Arabic.Resources;
using ArabicDLS;

namespace Filters
{
    /// <summary>
    /// فلتر للأحرف التي تحتوي حركات
    /// </summary>
    class DiacCharFilter : TokenFilter
    {
        public DiacCharFilter(TokenFilter tf) : this(tf._tmptoken)
        {

        }
        public DiacCharFilter(ArabicWord token) : base(token)
        {

            if (FirstFilter)
            {
                var tmp = token.GetArabicWordWithSokonByDefault;
                //اذا كان اول حرف متحرك
                if (tmp[0].HasDiacWithoutSokon)
                {
                    //جيب تاني حرف
                    if (tmp.Length > 1)
                    {
                        //if next char was a Vowel {ا و ي}
                        if (tmp[1].HasDiacWithoutSokon && tmp[0].Diac.AsEnum == tmp[1].Diac.AsEnum && !IsTheStart && ((tmp.Length > 2 && !tmp[2].IsVowel) || tmp.Length < 3))
                        {
                            syllables.Add(new Syllable(new Sign(tmp[0], Side.Left)), GetType().Name);
                            syllables.Add(new Syllable(new Sign(tmp[1], Side.Right)), GetType().Name);
                            _tmptoken = tmp.Remove(2);
                        }
                        else if (tmp[1].HasSokon && tmp.Length > 2 && !tmp[2].IsVowel)
                        {
                            if (tmp[0].Diac.AsEnum == Diacritics.fatha && tmp.Length > 2 && !tmp[2].IsVowel && tmp[1].Char != tmp[2].Char && !tmp[1].HasSokon)
                            {
                                var t = new ArabicChar(tmp[1].Char, new Diacritic(R.sokon), tmp[1].HasShadeh, tmp[1].ParentWord);
                                syllables.Add(new Syllable(new Sign(tmp[0], Side.Left)), GetType().Name);
                                syllables.Add(new Syllable(new Sign(t, Side.Right)), GetType().Name);
                            }
                            else
                            {
                                syllables.Add(new Syllable(new Sign(tmp[0], Side.Left)), GetType().Name);
                                var t = new ArabicChar(tmp[1].Char, new Diacritic(R.sokon), tmp[1].HasShadeh, tmp[1].ParentWord);
                                syllables.Add(new Syllable(new Sign(t, Side.Right)), GetType().Name);
                            }
                            _tmptoken = tmp.Remove(2);

                        }
                        else if (tmp[1].HasDiacWithoutSokon && tmp[0].Diac.AsEnum != tmp[1].Diac.AsEnum)
                        {
                            syllables.Add(new Syllable(new Sign(tmp[0], Side.Left)), GetType().Name);
                            syllables.Add(new Syllable(new Sign(tmp[1], Side.Right)), GetType().Name);
                            _tmptoken = tmp.Remove(2);
                        }
                        //كتب
                        else if (tmp.Length > 2 && tmp[0].Diac.AsEnum == Diacritics.fatha && tmp[1].Diac.AsEnum == Diacritics.fatha && tmp[2].Diac.AsEnum == Diacritics.fatha)
                        {
                            syllables.Add(new Syllable(new Sign(tmp[0], Side.Left), new Sign(tmp[1], Side.Right)), GetType().Name);
                            _tmptoken = tmp.Remove(2);
                        }
                        else if (tmp.Length == 2 && !tmp[1].HasDiacWithoutSokon)
                        {
                            syllables.Add(new Syllable(new Sign(tmp[0], Side.Left), new Sign(tmp[1], Side.Right)), GetType().Name);
                            _tmptoken = tmp.Remove(2);
                        }
                        //else if the char has Diac but after it doesn't not has sokon,same Diac,a Vowel
                        else
                        {
                            syllables.Add(new Syllable(new Sign(tmp[0], Side.Left)), GetType().Name);
                            _tmptoken = tmp.Remove(1);
                        }

                    }
                    //اذا ماكان في اي حرف بعده
                    else
                    {
                        syllables.Add(new Syllable(new Sign(tmp[0], Side.Left, false, true)), GetType().Name);
                        _tmptoken = tmp.Remove(1);
                    }
                    if (IsTheStart)
                        IsTheStart = false;
                    FirstFilter = false;
                }
                else if (tmp[0].HasSokon && tmp.Length > 1 && tmp[1].HasDiacWithoutSokon)
                {
                    syllables.Add(new Syllable(new Sign(tmp[0], Side.Left)), GetType().Name);
                    _tmptoken = tmp.Remove(1);

                    if (IsTheStart)
                        IsTheStart = false;
                    FirstFilter = false;
                }
            }
        }

    }
}
