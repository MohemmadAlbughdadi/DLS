﻿using ArabicDLS;
using Arabic;
using R = Arabic.Resources;

namespace Filters
{
    class ShadehFilter : TokenFilter
    {
        /// <summary>
        /// للأحرف التي تحتوي على الشدة حالة خاصة بالحاكي تبعنا ايضا
        /// </summary>
        /// <param name="tf"></param>
        public ShadehFilter(TokenFilter tf) : this(tf._tmptoken)
        {

        }
        public ShadehFilter(ArabicWord token) : base(token)
        {
            if (FirstFilter)
            {
                var tmp = token;
                if (tmp[0].HasShadeh || (tmp.Length > 1 && tmp[1].HasShadeh))
                {
                    var enterd = false;
                    if (tmp.Length > 1 && tmp[1].HasShadeh)
                    {
                        enterd = true;
                        var t = new ArabicChar(tmp[1].Char, new Diacritic(R.sokon), tmp[1].HasShadeh, tmp[1].ParentWord);
                        syllables.Add(new Syllable(new Sign(tmp[0], Side.Left), new Sign(t, Side.Right)),GetType().Name);
                        var tt = new ArabicChar(tmp[1].Char, tmp[1].Diac, false, tmp[1].ParentWord);

                        _tmptoken = token = tmp = tmp.Remove(2);

                    }
                    //اذا تم حذف اول حرف من التوكن
                    //اذكر هنا كان التعامل مع الأحرف الشمسية التي ليس عليها شدة او التي عليها شدة
                    if (enterd)
                    {
                        if (tmp.Length > 1)
                        {
                            syllables.Add(new Syllable(new Sign(tmp[0], Side.Left), new Sign(tmp[1], Side.Right)),GetType().Name);
                            _tmptoken = token.Remove(2);
                        }
                        else if (tmp.Length > 0)
                        {
                            syllables.Add(new Syllable(new Sign(tmp[0], Side.Left,false,true)),GetType().Name);
                            _tmptoken = token.Remove(1);
                        }
                    }
                    else
                    {
                        var t = new ArabicChar(tmp[0].Char, new Diacritic(R.sokon), tmp[0].HasShadeh, tmp[0].ParentWord);

                        if (tmp.Length >= 2)
                        {
                            syllables.Add(new Syllable(new Sign(t, Side.Right)),GetType().Name);
                            syllables.Add(new Syllable(new Sign(tmp[0], Side.Left), new Sign(tmp[1], Side.Right)),GetType().Name);
                            _tmptoken = token.Remove(2);
                        }
                        else if (tmp.Length > 0)
                        {
                            syllables.Add(new Syllable(new Sign(t, Side.Right)),GetType().Name);
                            syllables.Add(new Syllable(new Sign(tmp[0], Side.Left)),GetType().Name);
                            _tmptoken = token.Remove(1);
                        }
                    }
                    if (IsTheStart)
                        IsTheStart = false;
                    FirstFilter = false;
                }
            }
        }
    }
}
