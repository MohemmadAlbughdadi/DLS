﻿using Arabic;
using System;
using System.Collections.Generic;
using System.Linq;
using R = Arabic.Resources;
using SI = ArabicDLS.ImagesUrl;

namespace ArabicDLS
{
    public class Syllable
    {
        /// <summary>
        /// Get Syllable String
        /// </summary>
        public string syllable
        {
            get
            {
                var t = GetSigns().Select(x => x.AChar.Where(y => y != null));
                var tt = t.Select(x => string.Join("", x.Select(y => y.GetArabicCharString)));
                return string.Join("", tt);
            }
        }
        public Syllable Prev { get; set; }//Previous Syllable to this Syllable
        public Syllable Next { get; set; }//Next Syllable to this Syllable
        public bool IsFirstToken { get; set; }//check if this Syllable is in the first token{I needed this because some char Perform is defferient When is in first token}
        private Tuple<Sign, Sign> move;//This move


        public Syllable()
        {
            move = new Tuple<Sign, Sign>(null, null);
        }
        public Syllable(Sign s)
        {

            move = new Tuple<Sign, Sign>(s, null);
        }
        public Syllable(Sign s1, Sign s2)
        {

            move = new Tuple<Sign, Sign>(s1, s2);
        }
        public IEnumerable<Sign> GetSigns()
        {
            var signs = new List<Sign>();
            if (move.Item1 != null)
                signs.Add(move.Item1);
            if (move.Item2 != null)
                signs.Add(move.Item2);
            return signs;
        }
        /// <summary>
        /// This is the main Function to perform Syllabels of each token of sentnce
        /// </summary>
        /// <returns></returns>
        public Tuple<Tuple<string, string, string>, Tuple<string, string, string>> Perform_Syllable()
        {
            var signs = GetSigns();
            foreach (var item in signs)
            {
                item.IsFirstToken = IsFirstToken;
            }
            //if the syllable has one sign
            if (signs.Count() == 1)
            {
                signs.First().ConnectedToPrev = Prev != null ? Prev.GetSigns().Last().AChar.Where(x => x != null).Last().ConnectedToNext : false;
                signs.First().ConnectedToNext = Next != null ? Next.GetSigns().First().AChar.First().ConnectedToPrev : false;
                var TheSign = signs.First();
                var perform = TheSign.Perform_Sign();
                //because Haa Came Alone
                //if the side of the char is left
                if (TheSign._Side == Side.Right)
                {
                    return Tuple.Create(Tuple.Create(TheSign.Perform_Sign().Item1, TheSign.Perform_Sign().Item2, TheSign.GetSignDiacArrow()), Tuple.Create<string, string, string>(SI.StandBy, SI.StandBy, null));
                }
                //if the side of the char is right
                else if (TheSign._Side == Side.Left)
                {
                    return Tuple.Create(Tuple.Create<string, string, string>(SI.StandBy, SI.StandBy, null), Tuple.Create(TheSign.Perform_Sign().Item1, TheSign.Perform_Sign().Item2, TheSign.GetSignDiacArrow()));
                }
                //if the char drawn with both hands
                else if (TheSign._Side == Side.Both)
                {
                    //في حال كانت ال الشمسية
                    if (TheSign.AChar.Length == 3 && (TheSign.AChar[0].Char + TheSign.AChar[1].Char).StartsWith(R.alef + R.lam))
                    {
                        var x = new ArabicChar(TheSign.AChar[2].Char, new Diacritic(), false, TheSign.AChar[2].ParentWord);
                        var al = new Sign(x, Side.Right).Perform_Sign();
                        return Tuple.Create(Tuple.Create<string, string, string>(al.Item1, al.Item2, null), Tuple.Create(perform.Item1, perform.Item2, TheSign.GetSignDiacArrow()));
                    }
                    else//ماعدا ذلك
                        return Tuple.Create(Tuple.Create(perform.Item1, perform.Item2, TheSign.GetSignDiacArrow()), Tuple.Create<string, string, string>(null, null, null));
                }
                else//Side= _none
                    return Tuple.Create(Tuple.Create<string, string, string>(SI.StandBy, SI.StandBy, null), Tuple.Create<string, string, string>(SI.StandBy, SI.StandBy, null));
            }
            else if (signs.Count() == 2)
            {
                signs.First().ConnectedToNext = Prev != null ? Prev.GetSigns().Last().AChar.Where(x => x != null).Last().ConnectedToNext : false;
                signs.First().ConnectedToPrev = signs.ElementAt(1).AChar.First().ConnectedToPrev;

                signs.ElementAt(1).ConnectedToNext = signs.First().AChar.Where(x => x != null).Last().ConnectedToNext;
                signs.ElementAt(1).ConnectedToPrev = Next != null ? Next.GetSigns().First().AChar.First().ConnectedToPrev : false;
                var firstsign = signs.First();
                var perform = firstsign.Perform_Sign();
                if (firstsign._Side == Side.Right)
                {
                    return Tuple.Create(Tuple.Create(perform.Item1, perform.Item2, firstsign.GetSignDiacArrow()), Tuple.Create(signs.ElementAt(1).Perform_Sign().Item1, signs.ElementAt(1).Perform_Sign().Item2, signs.ElementAt(1).GetSignDiacArrow()));
                }
                else
                {
                    return Tuple.Create(Tuple.Create(signs.ElementAt(1).Perform_Sign().Item1, signs.ElementAt(1).Perform_Sign().Item2, signs.ElementAt(1).GetSignDiacArrow()), Tuple.Create(perform.Item1, perform.Item2, firstsign.GetSignDiacArrow()));
                }
            }
            //default if empty sign
            else
            {
                return Tuple.Create(Tuple.Create<string, string, string>(SI.StandBy, SI.StandBy, null), Tuple.Create<string, string, string>(SI.StandBy, SI.StandBy, null));
            }
        }
    }
}
