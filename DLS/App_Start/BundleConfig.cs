﻿using System.Web.Optimization;

namespace DLS
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/DLSScriptBundle").Include(
                        "~/Scripts/jquery-{version}.js", "~/Scripts/magnific-*", "~/Scripts/bootstrap.js", "~/Scripts/jquery.unobtrusive-ajax.js", "~/Scripts/jquery-ui-{version}.js", "~/Scripts/jquery.validate*", "~/Scripts/jquery.validate.unobtrusive*"));
            
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            
            bundles.Add(new StyleBundle("~/Content/DLSStyleBundle").Include("~/Content/bootstrap.css", "~/Content/bootstrap-theme.css", "~/Content/font-awesome.min.css", "~/Content/theme-style.css"));
            
            BundleTable.EnableOptimizations = true;

        }
    }
}