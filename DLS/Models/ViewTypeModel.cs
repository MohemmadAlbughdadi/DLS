﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DLS.Models
{
    public class ViewTypeModel
    {
        [Required, DataType(DataType.MultilineText)]
        public string InputTextArea { get; set; }
        [Range(0,5)]
        public int Range { get; set; }

        [Required, DataType(DataType.EmailAddress)]
        public string UserEmail { get; set; }
        [Required, DataType(DataType.Text), StringLength(maximumLength: 20, MinimumLength = 3)]
        public string UserName { get; set; }
        [Required, DataType(DataType.MultilineText), StringLength(maximumLength: 1000, MinimumLength = 20)]
        public string Message { get; set; }


    }
}