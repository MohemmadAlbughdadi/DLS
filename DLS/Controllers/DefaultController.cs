﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;
using DLS.Models;
using Newtonsoft.Json;

namespace DLS.Controllers
{
    public class DefaultController : Controller
    {
        public string WebApi = Configuration.Configuration.ApiUrl;
        // GET: Default

        public ActionResult Index()
        {

            return View();
        }
        /// <summary>
        /// Input is string OutPut is Signs draw on the Simulator
        /// </summary>
        /// <param name="inputTextArea"></param>
        /// <returns></returns>
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> Translate(string inputTextArea)
        {
            try
            {
                var tmpinput = inputTextArea;
                if (!ModelState.IsValid)
                {
                    ModelState.AddModelError("ErrorArea", "Invalid User Input.");
                    return View("index");
                }
                using (var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
                {
                    client.BaseAddress = new Uri(WebApi);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response = await client.PostAsJsonAsync("/api/signals/", new Content { input = tmpinput });
                    var tmp = response.Content.ReadAsAsync<TheResponse>().Result;
                    return Content(JsonConvert.SerializeObject(new { Syllables = tmp.List, Jism = tmp.jism }));
                }
            }
            catch
            {
                ModelState.AddModelError("ErrorArea", "Invalid User Input.");
                return View("index");
            }
        }
        /// <summary>
        /// Get a first position images of the simulator
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        
        public ActionResult StartUp()
        {
            try
            {
                using (var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
                {
                    client.BaseAddress = new Uri(WebApi);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.PostAsync("/api/signals/StartUp", null).Result.Content.ReadAsAsync<TheResponse>().Result;
                    var result = new { Syllables = response.List, Jism = response.jism };
                    return Json(result);
                }
            }
            catch
            {
                ModelState.AddModelError("ErrorArea", "Server Are Down Please Retry Later");
                return View("Index");
            }
        }
        /// <summary>
        /// Get a List of images to preload so the simulator works simentinously 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LoadImages()
        {
            try
            {
                using (var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
                {
                    client.BaseAddress = new Uri(WebApi);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.PostAsync("/api/signals/LoadImages", null).Result.Content.ReadAsAsync<TheResponse>().Result;
                    var result = new { Syllables = response.List.Select(x => x.Url1), Jism = response.jism };
                    return Json(result);
                }
            }
            catch
            {
                ModelState.AddModelError("ErrorArea", "Server Are Down Please Retry Later");
                return View("Index");
            }
        }

        /// <summary>
        /// Contact Us Form
        /// </summary>
        /// <param name="userName">User Full Name</param>
        /// <param name="userEmail">User Email</param>
        /// <param name="message">User Feedback</param>
        /// <returns></returns>
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ContactUs(string userName, string userEmail, string message)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("ErrorArea2", "Invalid User Input.");
                return View("Index");
            }
            using (var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
            {
                client.BaseAddress = new Uri(WebApi);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.PostAsJsonAsync("/api/contactus/", new { User = userName, Email = userEmail, Message = message });
                return View("Index");
            }

        }
    }
}