﻿namespace DeafLanguageSimulator
{
    partial class LoaderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ImagesLoadingBar = new System.Windows.Forms.ProgressBar();
            this.Wait_Text = new System.Windows.Forms.Label();
            this.FileLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ImagesLoadingBar
            // 
            this.ImagesLoadingBar.Location = new System.Drawing.Point(12, 226);
            this.ImagesLoadingBar.Name = "ImagesLoadingBar";
            this.ImagesLoadingBar.Size = new System.Drawing.Size(260, 23);
            this.ImagesLoadingBar.TabIndex = 0;
            // 
            // Wait_Text
            // 
            this.Wait_Text.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Wait_Text.Font = new System.Drawing.Font("Calibri", 15F);
            this.Wait_Text.Location = new System.Drawing.Point(12, 37);
            this.Wait_Text.Name = "Wait_Text";
            this.Wait_Text.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Wait_Text.Size = new System.Drawing.Size(235, 89);
            this.Wait_Text.TabIndex = 1;
            this.Wait_Text.Text = "يرجى الإنتظار ريثما يتم تحميل الملفات المطلوبة";
            this.Wait_Text.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FileLabel
            // 
            this.FileLabel.AutoSize = true;
            this.FileLabel.Location = new System.Drawing.Point(51, 195);
            this.FileLabel.Name = "FileLabel";
            this.FileLabel.Size = new System.Drawing.Size(0, 13);
            this.FileLabel.TabIndex = 2;
            // 
            // LoaderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.FileLabel);
            this.Controls.Add(this.Wait_Text);
            this.Controls.Add(this.ImagesLoadingBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "LoaderForm";
            this.Text = "LoaderForm";
            this.Load += new System.EventHandler(this.LoaderForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label Wait_Text;
        public System.Windows.Forms.ProgressBar ImagesLoadingBar;
        private System.Windows.Forms.Label FileLabel;
    }
}