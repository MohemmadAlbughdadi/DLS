﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeafLanguageSimulator
{
    public partial class LoaderForm : Form
    {
        public LoaderForm()
        {
            InitializeComponent();
        }

        private void LoaderForm_Load(object sender, EventArgs e)
        {

            if (MessageBox.Show("هل تريد اعادة تحديث جميع الصور؟", "موافقة تحديث", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (Directory.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images")))
                    Directory.Delete(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images"), true);
            }
            if (!Directory.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images")))
            {
                Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images"));
            }
            Task.Factory.StartNew(() =>
            {
                /*Controller.PreloadImages(ImagesLoadingBar, FileLabel); Hide();*/ var form = new MainForm();
                form.ShowDialog();
            });


        }
    }
}
