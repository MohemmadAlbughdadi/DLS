﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DLS.Models
{
    [DataContract]
    public class Response
    {
        [DataMember(IsRequired = true, Order = 1)]
        public string Url1 = null;
        [DataMember(IsRequired = true, Order = 2)]
        public string Url1_Sokon = null;
        [DataMember(IsRequired = true, Order = 3)]

        public string Diac1 = null;
        [DataMember(IsRequired = true, Order = 4)]

        public string Url2 = null;
        [DataMember(IsRequired = true, Order = 5)]

        public string Url2_Sokon = null;

        [DataMember(IsRequired = true, Order = 6)]

        public string Diac2 = null;


        [DataMember(IsRequired = true, Order = 7)]

        public string The_Syl = null;
        public Response(string url1, string url1_Sokon, string diac1, string url2, string url2_Sokon, string diac2, string the_syl)
        {
            Url1 = url1;
            Url1_Sokon = url1_Sokon;
            Diac1 = diac1;
            Diac2 = diac2;
            Url2 = url2;
            Url2_Sokon = url2_Sokon;
            The_Syl = the_syl;
        }
    }
    [DataContract]
    public class Content
    {
        [DataMember(IsRequired = true, Order = 1)]
        public string input;
    }
    [DataContract]
    public class TheResponse
    {
        [DataMember(IsRequired = true, Order = 1)]

        public List<Response> List;
        [DataMember(IsRequired = true, Order = 2)]

        public string jism;
    }
}