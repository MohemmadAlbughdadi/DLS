﻿using DLS.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeafLanguageSimulator
{
    public partial class MainForm : Form
    {
        int Delay = 1;
        TheResponse TR = null;
        public MainForm()
        {

            InitializeComponent();
            var TranslatePos = this.PointToScreen(TranslateLabel.Location);
            TranslatePos = SpeechBubble.PointToClient(TranslatePos);
            TranslateLabel.BackColor = Color.Transparent;

            TranslateLabel.Parent = SpeechBubble;
            TranslateLabel.Location = TranslatePos;

            var SyllabelPos = this.PointToScreen(SyllabelsLabel.Location);
            SyllabelPos = SpeechBubble.PointToClient(SyllabelPos);
            SyllabelsLabel.BackColor = Color.Transparent;

            SyllabelsLabel.Parent = SpeechBubble;
            SyllabelsLabel.Location = SyllabelPos;


            Controller.StartUp(LeftHandPicture, RightHandPicture, JismPicture);


            JismPicture.Controls.Add(RightHandPicture);
            RightHandPicture.Controls.Add(LeftHandPicture);
            LeftHandPicture.Controls.Add(RightArrow);
            LeftHandPicture.Controls.Add(LeftArrow);

            RightHandPicture.Location = new Point(0, 0);
            LeftHandPicture.Location = new Point(0, 0);
            RightHandPicture.BackColor = Color.Transparent;
            LeftArrow.BackColor = Color.Transparent;
            RightArrow.BackColor = Color.Transparent;
            LeftHandPicture.BackColor = Color.Transparent;
            if (RightHandPicture.Image != null)
            {
                Image img = RightHandPicture.Image;
                img.RotateFlip(RotateFlipType.Rotate180FlipY);
                RightHandPicture.Image = img;
            }
        }

        private void Translate_Button_Click(object sender, EventArgs e)
        {
            Translate_Button.Enabled = false;
            TR = Controller.Translate(InputBox.Text);
            Translate();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private async void Translate()
        {
            foreach (var item in TR.List)
            {
                TranslateLabel.Text = "يتم الأن معالجة المقطع";
                SyllabelsLabel.Text = item.The_Syl + "      ";
                RightArrow.Image = null;
                LeftArrow.Image = null;
                RightHandPicture.Image = null;
                LeftHandPicture.Image = null;


                if (!string.IsNullOrEmpty(item.Url2_Sokon))
                {

                    RightHandPicture.Image = new Bitmap(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images", Path.GetFileName(item.Url2_Sokon)));
                    Image img = RightHandPicture.Image;
                    img.RotateFlip(RotateFlipType.Rotate180FlipY);
                    RightHandPicture.Image = img;
                }
                LeftHandPicture.Image = new Bitmap(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images", Path.GetFileName(item.Url1_Sokon)));


                await Task.Delay(500);
                if (!string.IsNullOrEmpty(item.Diac2))
                    RightArrow.Image = new Bitmap(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images", Path.GetFileName(item.Diac2)));

                if (!string.IsNullOrEmpty(item.Url2))
                {
                    RightHandPicture.Image = new Bitmap(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images", Path.GetFileName(item.Url2)));
                    Image img = RightHandPicture.Image;
                    img.RotateFlip(RotateFlipType.Rotate180FlipY);
                    RightHandPicture.Image = img;
                }

                if (!string.IsNullOrEmpty(item.Diac1))
                    LeftArrow.Image = new Bitmap(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images", Path.GetFileName(item.Diac1)));

                LeftHandPicture.Image = new Bitmap(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images", Path.GetFileName(item.Url1)));


                await Task.Delay(Delay * 500);

            }
            TranslateLabel.Text = "جربني مرة اخرى";
            Translate_Button.Enabled = true;
        }
        private void SpeedBar_Scroll(object sender, EventArgs e)
        {
            Delay = SpeedBar.Value;
        }
    }
}
