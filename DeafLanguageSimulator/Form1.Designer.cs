﻿using System.Drawing;

namespace DeafLanguageSimulator
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.Translate_Button = new System.Windows.Forms.Button();
            this.SpeedBar = new System.Windows.Forms.TrackBar();
            this.InputBox = new System.Windows.Forms.TextBox();
            this.TranslateLabel = new System.Windows.Forms.Label();
            this.SyllabelsLabel = new System.Windows.Forms.Label();
            this.JismPicture = new System.Windows.Forms.PictureBox();
            this.RightHandPicture = new System.Windows.Forms.PictureBox();
            this.LeftHandPicture = new System.Windows.Forms.PictureBox();
            this.Fast_Label = new System.Windows.Forms.Label();
            this.Slow_Label = new System.Windows.Forms.Label();
            this.MainLabel = new System.Windows.Forms.Label();
            this.LeftArrow = new System.Windows.Forms.PictureBox();
            this.RightArrow = new System.Windows.Forms.PictureBox();
            this.SpeechBubble = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.SpeedBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.JismPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightHandPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftHandPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftArrow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightArrow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpeechBubble)).BeginInit();
            this.SuspendLayout();
            // 
            // Translate_Button
            // 
            this.Translate_Button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(85)))), ((int)(((byte)(128)))));
            this.Translate_Button.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.Translate_Button.ForeColor = System.Drawing.Color.White;
            this.Translate_Button.Location = new System.Drawing.Point(786, 319);
            this.Translate_Button.Name = "Translate_Button";
            this.Translate_Button.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Translate_Button.Size = new System.Drawing.Size(115, 53);
            this.Translate_Button.TabIndex = 0;
            this.Translate_Button.Text = "ترجم";
            this.Translate_Button.UseVisualStyleBackColor = false;
            this.Translate_Button.Click += new System.EventHandler(this.Translate_Button_Click);
            // 
            // SpeedBar
            // 
            this.SpeedBar.LargeChange = 3;
            this.SpeedBar.Location = new System.Drawing.Point(252, 327);
            this.SpeedBar.Maximum = 5;
            this.SpeedBar.Minimum = 1;
            this.SpeedBar.Name = "SpeedBar";
            this.SpeedBar.Size = new System.Drawing.Size(335, 45);
            this.SpeedBar.TabIndex = 1;
            this.SpeedBar.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.SpeedBar.Value = 1;
            this.SpeedBar.Scroll += new System.EventHandler(this.SpeedBar_Scroll);
            // 
            // InputBox
            // 
            this.InputBox.Location = new System.Drawing.Point(608, 48);
            this.InputBox.Multiline = true;
            this.InputBox.Name = "InputBox";
            this.InputBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.InputBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.InputBox.Size = new System.Drawing.Size(293, 265);
            this.InputBox.TabIndex = 2;
            // 
            // TranslateLabel
            // 
            this.TranslateLabel.AutoSize = true;
            this.TranslateLabel.BackColor = System.Drawing.Color.Transparent;
            this.TranslateLabel.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TranslateLabel.ForeColor = System.Drawing.Color.Black;
            this.TranslateLabel.Location = new System.Drawing.Point(348, 87);
            this.TranslateLabel.Name = "TranslateLabel";
            this.TranslateLabel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.TranslateLabel.Size = new System.Drawing.Size(168, 24);
            this.TranslateLabel.TabIndex = 5;
            this.TranslateLabel.Text = "اهلا بكم ببرنامج المحاكي";
            this.TranslateLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SyllabelsLabel
            // 
            this.SyllabelsLabel.AutoSize = true;
            this.SyllabelsLabel.BackColor = System.Drawing.Color.Transparent;
            this.SyllabelsLabel.Font = new System.Drawing.Font("Calibri", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SyllabelsLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(131)))), ((int)(((byte)(211)))));
            this.SyllabelsLabel.Location = new System.Drawing.Point(384, 131);
            this.SyllabelsLabel.Name = "SyllabelsLabel";
            this.SyllabelsLabel.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.SyllabelsLabel.Size = new System.Drawing.Size(78, 24);
            this.SyllabelsLabel.TabIndex = 6;
            this.SyllabelsLabel.Text = "للغة الصم";
            this.SyllabelsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // JismPicture
            // 
            this.JismPicture.BackColor = System.Drawing.Color.Transparent;
            this.JismPicture.Location = new System.Drawing.Point(12, 48);
            this.JismPicture.Name = "JismPicture";
            this.JismPicture.Size = new System.Drawing.Size(222, 324);
            this.JismPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.JismPicture.TabIndex = 7;
            this.JismPicture.TabStop = false;
            // 
            // RightHandPicture
            // 
            this.RightHandPicture.BackColor = System.Drawing.Color.Transparent;
            this.RightHandPicture.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.RightHandPicture.InitialImage = null;
            this.RightHandPicture.Location = new System.Drawing.Point(12, 48);
            this.RightHandPicture.Name = "RightHandPicture";
            this.RightHandPicture.Size = new System.Drawing.Size(222, 324);
            this.RightHandPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.RightHandPicture.TabIndex = 9;
            this.RightHandPicture.TabStop = false;
            // 
            // LeftHandPicture
            // 
            this.LeftHandPicture.BackColor = System.Drawing.Color.Transparent;
            this.LeftHandPicture.InitialImage = null;
            this.LeftHandPicture.Location = new System.Drawing.Point(12, 48);
            this.LeftHandPicture.Name = "LeftHandPicture";
            this.LeftHandPicture.Size = new System.Drawing.Size(222, 324);
            this.LeftHandPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.LeftHandPicture.TabIndex = 8;
            this.LeftHandPicture.TabStop = false;
            // 
            // Fast_Label
            // 
            this.Fast_Label.AutoSize = true;
            this.Fast_Label.Location = new System.Drawing.Point(249, 311);
            this.Fast_Label.Name = "Fast_Label";
            this.Fast_Label.Size = new System.Drawing.Size(33, 13);
            this.Fast_Label.TabIndex = 10;
            this.Fast_Label.Text = "اسرع";
            // 
            // Slow_Label
            // 
            this.Slow_Label.AutoSize = true;
            this.Slow_Label.Location = new System.Drawing.Point(555, 311);
            this.Slow_Label.Name = "Slow_Label";
            this.Slow_Label.Size = new System.Drawing.Size(32, 13);
            this.Slow_Label.TabIndex = 11;
            this.Slow_Label.Text = "ابطئ";
            // 
            // MainLabel
            // 
            this.MainLabel.AutoSize = true;
            this.MainLabel.Font = new System.Drawing.Font("Calibri", 20F);
            this.MainLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(85)))), ((int)(((byte)(128)))));
            this.MainLabel.Location = new System.Drawing.Point(268, 9);
            this.MainLabel.Name = "MainLabel";
            this.MainLabel.Size = new System.Drawing.Size(319, 33);
            this.MainLabel.TabIndex = 12;
            this.MainLabel.Text = "نظام المحاكي الإشاري للغتنا العربية";
            // 
            // LeftArrow
            // 
            this.LeftArrow.Location = new System.Drawing.Point(34, 105);
            this.LeftArrow.Name = "LeftArrow";
            this.LeftArrow.Size = new System.Drawing.Size(73, 50);
            this.LeftArrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.LeftArrow.TabIndex = 13;
            this.LeftArrow.TabStop = false;
            // 
            // RightArrow
            // 
            this.RightArrow.Location = new System.Drawing.Point(136, 105);
            this.RightArrow.Name = "RightArrow";
            this.RightArrow.Size = new System.Drawing.Size(73, 50);
            this.RightArrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.RightArrow.TabIndex = 14;
            this.RightArrow.TabStop = false;
            // 
            // SpeechBubble
            // 
            this.SpeechBubble.Image = ((System.Drawing.Image)(resources.GetObject("SpeechBubble.Image")));
            this.SpeechBubble.Location = new System.Drawing.Point(284, 48);
            this.SpeechBubble.Name = "SpeechBubble";
            this.SpeechBubble.Size = new System.Drawing.Size(287, 145);
            this.SpeechBubble.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.SpeechBubble.TabIndex = 15;
            this.SpeechBubble.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(913, 384);
            this.Controls.Add(this.SpeechBubble);
            this.Controls.Add(this.RightArrow);
            this.Controls.Add(this.LeftArrow);
            this.Controls.Add(this.MainLabel);
            this.Controls.Add(this.Slow_Label);
            this.Controls.Add(this.Fast_Label);
            this.Controls.Add(this.JismPicture);
            this.Controls.Add(this.RightHandPicture);
            this.Controls.Add(this.LeftHandPicture);
            this.Controls.Add(this.SyllabelsLabel);
            this.Controls.Add(this.TranslateLabel);
            this.Controls.Add(this.InputBox);
            this.Controls.Add(this.SpeedBar);
            this.Controls.Add(this.Translate_Button);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "نظام المحاكي الإشاري للغتنا العربية";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.SpeedBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.JismPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightHandPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftHandPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftArrow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightArrow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpeechBubble)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Translate_Button;
        private System.Windows.Forms.TrackBar SpeedBar;
        private System.Windows.Forms.TextBox InputBox;
        private System.Windows.Forms.Label SyllabelsLabel;
        private System.Windows.Forms.PictureBox JismPicture;
        private System.Windows.Forms.PictureBox LeftHandPicture;
        private System.Windows.Forms.PictureBox RightHandPicture;
        private System.Windows.Forms.Label Fast_Label;
        private System.Windows.Forms.Label Slow_Label;
        private System.Windows.Forms.Label MainLabel;
        private System.Windows.Forms.PictureBox LeftArrow;
        private System.Windows.Forms.PictureBox RightArrow;
        private System.Windows.Forms.PictureBox SpeechBubble;
        private System.Windows.Forms.Label TranslateLabel;
    }
}

