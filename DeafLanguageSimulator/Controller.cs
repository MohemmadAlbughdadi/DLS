﻿using DLS.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace DeafLanguageSimulator
{
    public static class Controller
    {
        public static string WebApi = @"http://deaf-api.azurewebsites.net";
        private static void DownloadRemoteImageFile(string uri)
        {
            if (!File.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images", Path.GetFileName(uri))))
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                // Check that the remote file was found. The ContentType
                // check is performed since a request for a non-existent
                // image file might be redirected to a 404-page, which would
                // yield the StatusCode "OK", even though the image was not
                // found.
                if ((response.StatusCode == HttpStatusCode.OK ||
                    response.StatusCode == HttpStatusCode.Moved ||
                    response.StatusCode == HttpStatusCode.Redirect) &&
                    response.ContentType.StartsWith("image", StringComparison.OrdinalIgnoreCase))
                {

                    // if the remote file was found, download it
                    using (Stream inputStream = response.GetResponseStream())
                    {
                        File.WriteAllBytes(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images", Path.GetFileName(uri)), ReadFully(inputStream));
                    }
                }
            }
        }
        private static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public static void PreloadImages(System.Windows.Forms.ProgressBar Pb, System.Windows.Forms.Label FL)
        {
            IEnumerable<string> Image_List = new List<string>();
            try
            {

                using (var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
                {
                    client.BaseAddress = new Uri(WebApi);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.PostAsync("/api/signals/LoadImages", null).Result.Content.ReadAsAsync<TheResponse>().Result;
                    Image_List = response.List.Select(x => x.Url1);
                }
            }
            catch
            {

            }
            var webclient = new WebClient();
           // Pb.Maximum = Image_List.Count();
            foreach (var item in Image_List)
            {
               // FL.Text = item;
                DownloadRemoteImageFile(WebApi + item);
                //Pb.Increment(1);

            }

        }

        public static void StartUp(System.Windows.Forms.PictureBox Left, System.Windows.Forms.PictureBox Right, System.Windows.Forms.PictureBox Jism)
        {
            try
            {
                using (var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
                {
                    client.BaseAddress = new Uri(WebApi);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.PostAsync("/api/signals/StartUp", null).Result.Content.ReadAsAsync<TheResponse>().Result;
                    Left.Image = new Bitmap(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images", Path.GetFileName(response.List.First().Url1)));
                    Right.Image = new Bitmap(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images", Path.GetFileName(response.List.First().Url2)));
                    Jism.Image = new Bitmap(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images", Path.GetFileName(response.jism)));
                }
            }
            catch { }
        }
        public static TheResponse Translate(string input)
        {
            try
            {
                using (var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
                {
                    client.BaseAddress = new Uri(WebApi);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.PostAsJsonAsync("/api/signals/", new Content { input = input }).Result;
                    var content = response.Content.ReadAsAsync<TheResponse>().Result;
                    return content;
                }
            }
            catch (Exception)
            {
                return new TheResponse();
            }

        }
        public static bool Check_Api()
        {
            return true;
        }

    }
}
