﻿using Arabic;
namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var ac = new ArabicChar("ال", Diacritics.ddameh);
            var x1 = "هلو";
            var x2 = "هَلَوِ";
            var x3 = "هِلّوْ";
            var x4 = "هَلَّوَّ";
            var x5 = "هٌلٌوِ";
            var x6 = "انا";
            var a1 = new ArabicWord(x1);
            var a2 = new ArabicWord(x2);
            var a3 = new ArabicWord(x3);
            var a4 = new ArabicWord(x4);
            var a5 = new ArabicWord(x5);
            var a6 = new ArabicWord(x6);
        }
    }
}
