﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using DSL_Api.Models;
using System.Collections;
using ArabicDLS;
using System.Globalization;
using System.Security.Cryptography;
using System.Data.Entity;

namespace DSL_Api.App_Start
{
    /// <summary>
    /// First i needed this to auto update images when they have any changes but then i was going to replace it with version checker
    /// The idea we will place the images version number in the database so everytime the program start it will check the version and if is higher it will auto update images
    /// so no need for this you can safly delete
    /// </summary>
    public class ImagesFileWatcher
    {

        FileSystemWatcher watcher = null;
        DLS_DBEntities db = new DLS_DBEntities();
        public ImagesFileWatcher()
        {
            update();
            watcher = new FileSystemWatcher();
            watcher.Path = "/Resources/";
            watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;
            watcher.Filter = "*.png";

            // Add event handlers.
            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.Created += new FileSystemEventHandler(OnChanged);
            watcher.Deleted += new FileSystemEventHandler(OnChanged);

        }
        protected void OnChanged(object source, FileSystemEventArgs e)
        {
            update();
        }
        public void update()
        {
            var List = ImagesUrl.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true).OfType<DictionaryEntry>().Select(x => new Tuple<string, string>(Path.GetFileName(x.Value.ToString()), (File.Exists(Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).FullName, x.Value.ToString())) ? Convert.ToBase64String(new SHA1Managed().ComputeHash(Convert.FromBase64String(File.GetLastWriteTimeUtc(Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).FullName, x.Value.ToString())).ToString()))) : null)));
            foreach (var item in List)
            {
                var tmp = db.ImageHash.SingleOrDefault(x => x.Name == item.Item1);
                if (tmp == null)
                {
                    db.ImageHash.Add(new ImageHash() { Name = item.Item1, Hash = item.Item2 });
                }
                else
                {
                    if (!tmp.Hash.Equals(item.Item2))
                    {
                        tmp.Hash = item.Item2;
                        db.Entry(tmp).State = EntityState.Modified;
                    }
                }
            }
            db.SaveChanges();

        }
    }
}