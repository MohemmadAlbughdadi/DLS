﻿using ArabicDLS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using DSL_Api.Models;
using System.Globalization;
using System.Collections;
using Arabic;
namespace DSL_Api.Controllers
{
    [RoutePrefix("api/signals")]
    public class SignalsController : ApiController
    {
        [Route("Check"), HttpGet]
        public IHttpActionResult Check()
        {
            try
            {
                return Ok("This Api Works Perfectly");

            }
            catch (Exception)
            {
                return NotFound();

            }
        }
        /// <summary>
        /// Deal with sartup function in the website this will return the first position of the simulator
        /// </summary>
        /// <returns></returns>
        [Route("StartUp"), HttpPost]
        public IHttpActionResult StartUp()
        {
            try
            {
                return Ok(new
                {
                    List = new List<Response>() { new Response(ImagesUrl.StandBy, ImagesUrl.StandBy, null, ImagesUrl.StandBy, ImagesUrl.StandBy, null, "") },
                    Jism = ImagesUrl.Jism_Salah
                });
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
        /// <summary>
        /// Will get a list of images to preload the simulator to make it works with no interupt
        /// </summary>
        /// <returns></returns>
        [Route("LoadImages"), HttpPost]
        public IHttpActionResult LoadImages()
        {
            try
            {
                return Ok(new
                {
                    List = ImagesUrl.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true).OfType<DictionaryEntry>().Select(x => new Response(x.Value.ToString(), null, null, null, null, null, null)),
                    Jism = ImagesUrl.Jism_Salah
                });
            }
            catch (Exception)
            {
                return NotFound();

            }
        }

        /* public static byte[] ImageToByte(Image img)
         {
             using (var stream = new MemoryStream())
             {
                 img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                 return stream.ToArray();
             }
         }*/
        [ResponseType(typeof(TheResponse))]
        public IHttpActionResult PostSignals(Content c)
        {
            try
            {
                return Ok(new TheResponse { List = ProceedOperation(c.input), jism = ImagesUrl.Jism_Salah });
            }
            catch
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Will call ArabicDls layer to make the program works
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private List<Response> ProceedOperation(string input)
        {
            List<Response> Response_List = new List<Response>();

            var tokenizer = new Tokenizer(input);
            var syllables = tokenizer.Run();
            foreach (var syllable in syllables)
            {
                try
                {
                    var tmp = syllable.Perform_Syllable();
                    var t = string.Join("", syllable.GetSigns().Select(x => x.AChar.ArCharArrayToString()));
                    Response_List.Add(new Response(tmp.Item1.Item1, tmp.Item1.Item2, tmp.Item1.Item3, tmp.Item2.Item1, tmp.Item2.Item2, tmp.Item2.Item3, t));
                }
                catch (Exception ex)
                {

                }
            }
            return Response_List;
        }
    }
}
