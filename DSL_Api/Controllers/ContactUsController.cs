﻿using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using DSL_Api.Models;

namespace DSL_Api.Controllers
{
    public class ContactUsController : ApiController
    {
        private readonly DLS_DBEntities _db = new DLS_DBEntities();


        [ResponseType(typeof(ContactUs))]
        public async Task<IHttpActionResult> GetContactUs(int id)
        {
            var tmp = await _db.ContactUs.FindAsync(id);

            return Ok(tmp);
        }
        // POST: api/ContactUs
        [ResponseType(typeof(ContactUs))]
        public async Task<IHttpActionResult> PostContactUs(ContactUs contactUs)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _db.ContactUs.Add(contactUs);


            try
            {
                await _db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ContactUsExists(contactUs.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = contactUs.Id }, contactUs);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ContactUsExists(int id)
        {
            return _db.ContactUs.Count(e => e.Id == id) > 0;
        }
    }
}