﻿using DSL_Api.App_Start;
using System.Web.Http;
using System.Web.Routing;
namespace DSL_Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
          //  GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings = new Newtonsoft.Json.JsonSerializerSettings();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

        }
    }
}
