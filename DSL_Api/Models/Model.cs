﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace DSL_Api.Models
{
    [DataContract]
    public class Response
    {
        [DataMember(IsRequired = true, Order = 1)]
        public string Url1 = null;
        [DataMember(IsRequired = true, Order = 2)]
        public string Url1_Sokon = null;
        [DataMember(IsRequired = true, Order = 3)]

        public string Diac1 = null;
        [DataMember(IsRequired = true, Order = 4)]

        public string Url2 = null;
        [DataMember(IsRequired = true, Order = 5)]

        public string Url2_Sokon = null;
        
        [DataMember(IsRequired = true, Order = 6)]

        public string Diac2 = null;


        [DataMember(IsRequired = true, Order = 7)]

        public string The_Syl = null;
        public Response(string url1, string url1_Sokon,string diac1, string url2, string url2_Sokon, string diac2, string the_syl)
        {
            Url1 = url1;
            Url1_Sokon = url1_Sokon;
            Diac1 = diac1;
            Url2 = url2;
            Url2_Sokon = url2_Sokon;
            Diac2 = diac2;
            The_Syl = the_syl;
        }
    }


    [DataContract]
    public class TheResponse
    {
        [DataMember(IsRequired = true, Order = 1)]

        public List<Response> List;
        [DataMember(IsRequired = true, Order = 2)]

        public string jism;
    }
    [DataContract]
    public class Content
    {
        [DataMember(IsRequired = true, Order = 1)]
        public string input;
    }
    
    public partial class ContactUsMetaModel {
        [StringLength(maximumLength: 20, MinimumLength = 3), DataType(DataType.Text)]
        public string User;
        [Required, EmailAddress, DataType(DataType.EmailAddress)]
        public string Email;
        [Required, DataType(DataType.MultilineText)]
        public string Message;
    }
    [MetadataType(typeof(ContactUsMetaModel))]
    public partial class ContactUs { }
}