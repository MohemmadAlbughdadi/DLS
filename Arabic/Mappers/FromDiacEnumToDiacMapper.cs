﻿using R = Arabic.Resources;
namespace Arabic.Mappers
{
    public static class FromDiacEnumToDiacMapper
    {
        /// <summary>
        /// Mapper From Diac Enumeration to Diac object
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        public static Diacritic FromDiacEnumToDiac(Arabic.Diacritics d)
        {
            var tmp = new Diacritic();
            switch (d)
            {
                case Diacritics.fatha:
                    tmp.Diac = R.fatha;
                    break;
                case Diacritics.kasra:
                    tmp.Diac = R.kasra;
                    break;
                case Diacritics.ddameh:
                    tmp.Diac = R._9ameh;
                    break;
                case Diacritics.sokon:
                    tmp.Diac = R.sokon;
                    break;
                case Diacritics.tnoen_kasra:
                    tmp.Diac = R.tnoen_kasra;
                    break;
                case Diacritics.tnoen_ddameh:
                    tmp.Diac = R.tnoen_9amma;
                    break;
                case Diacritics.tnoen_fatha:
                    tmp.Diac = R.tnoen_fatha;
                    break;
                default:
                    break;
            }
            return tmp;
        }
    }
}
