﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using R = Arabic.Resources;

namespace Arabic.Mappers
{
    /// <summary>
    /// Mapper from Diact string to Diact object
    /// </summary>
    public static class FromDiacToDiactrectMapper
    {
        public static Diacritic FromDiacChar(string _Diac)
        {
            Diacritic Diac = new Diacritic()
            {
                Diac = _Diac
            };
            return Diac;
        }
    }
}
