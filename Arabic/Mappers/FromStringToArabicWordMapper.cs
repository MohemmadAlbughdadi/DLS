﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Arabic.Mappers
{
    public static class FromStringToArabicWordMapper
    {
        /// <summary>
        /// This is the main core function that will parse string to ArabicWord
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        public static ArabicWord FromString(string word)
        {
            ArabicWord AW = new ArabicWord();

            if (!string.IsNullOrWhiteSpace(word))
            {
                List<ArabicChar> ACL = new List<ArabicChar>();
                var subword = getArabicCharSubString(word, 0);

                for (int i = 0; i < subword.Item2; i++)
                {
                    var ac = new ArabicChar(AW);
                    if (i != 0)
                        subword = getArabicCharSubString(word, i);

                    foreach (var c in subword.Item1)
                    {
                        if (ArabicChar.CheckIfDiac(c.ToString()))
                        {
                            ac.Diac = FromDiacToDiactrectMapper.FromDiacChar(c.ToString());
                        }
                        else if (ArabicChar.CheckIfShadeh(c.ToString()))
                        {
                            ac.HasShadeh = true;
                        }
                        else if (ArabicChar.CheckIfArabicChar(c.ToString()))
                        {
                            ac.Char = c.ToString();
                        }
                    }
                    ACL.Add(ac);
                }

                AW.Word = ACL;

            }
            return AW;

        }
        /// <summary>
        /// This will return ArabicChar with all it Diact and shadeh
        /// </summary>
        /// <param name="word"></param>
        /// <param name="charnum"></param>
        /// <returns></returns>
        private static Tuple<string, int> getArabicCharSubString(string word, int charnum)
        {
            List<string> tmp = new List<string>();
            StringBuilder sb = new StringBuilder();
            bool b = false;
            for (int i = 0; i < word.Length; i++)
            {
                if (ArabicChar.CheckIfArabicChar(word[i].ToString()) && b)
                {
                    tmp.Add(sb.ToString());
                    sb = new StringBuilder();
                }
                if (ArabicChar.CheckIfArabicChar(word[i].ToString()) && !b)
                {
                    b = true;
                }
                sb.Append(word[i]);
                if (i == word.Length-1)
                    tmp.Add(sb.ToString());
            }
            return Tuple.Create(tmp.ElementAtOrDefault(charnum), tmp.Count());
        }
        public static IEnumerable<string> SplitAndKeep(this string s, char[] delims)
        {
            int start = 0, index;

            while ((index = s.IndexOfAny(delims, start)) != -1)
            {
                if (index - start > 0)
                    yield return s.Substring(start, index - start);
                yield return s.Substring(index, 1);
                start = index + 1;
            }

            if (start < s.Length)
            {
                yield return s.Substring(start);
            }
        }
    }

}



/*
                foreach (var c in word)
                {
                    var ac = new ArabicChar();

                    //if statments order is important
                    if (ArabicChar.CheckIfDiac(c.ToString()))
                    {
                        ac.Diac = ac.Diac ?? FromDiacToDiactrectMapper.FromDiacChar(c.ToString());
                        finishchar = false;
                    }
                    else if (ArabicChar.CheckIfShadeh(c.ToString()))
                    {
                        ac.HasShadeh = ac.HasShadeh || true;
                        finishchar = false;
                    }
                    else if (ArabicChar.CheckIfArabicChar(c.ToString()))
                    {
                        if (finishchar)
                        {
                            finishchar = false;
                            ACL.Add(ac);
                        }
                        else
                        {
                            finishchar = true;
                        }
                        ac.Char = c.ToString();
                    }
                }
                if (finishchar && word.Length > 0)
                {
                    ACL.Add(ac);
                }

                AW.Word = ACL;
            }
            return AW;*/
